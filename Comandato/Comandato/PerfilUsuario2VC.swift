//
//  PerfilUsuario2VC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 23/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import AMPopTip
import PureJsonSerializer
import ImageIO
import AVFoundation

class PerfilUsuario2VC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var imgUsuario: UIImageView!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var txtCedula: UITextField!
    @IBOutlet weak var txtCelular: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtFNacimiento: UITextField!
    @IBOutlet weak var txtCorreoe: UITextField!
    @IBOutlet weak var txtDireccionCasa: UITextField!
    @IBOutlet weak var txtDireccionTrabajo: UITextField!
    @IBOutlet weak var btnEditar: UIButton!
    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblNombreCompleto: UILabel!
    @IBOutlet weak var lblCedula: UILabel!
    @IBOutlet weak var lblCorreoUsuario: UILabel!
    @IBOutlet weak var stackCampos: UIStackView!
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var datos_usuario : datosUsuario = datosUsuario()
    
    var usr_id = ""
    
    //array para los errores
    var boolsError: [String:Bool] = ["nombre":false,"cedula":false,"correo":false,"celular":false,"dirTrabajo":false]
    
    //image picker
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setear la imagen de usuario
        self.imgUsuario.hidden = true
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
        //configuracion de controles
        self.txtNombre.delegate = self
        self.txtApellidos.delegate = self
        self.txtCedula.delegate = self
        self.txtCelular.delegate = self
        self.txtTelefono.delegate = self
        self.txtFNacimiento.delegate = self
        self.txtCorreoe.delegate = self
        self.txtDireccionCasa.delegate = self
        self.txtDireccionTrabajo.delegate = self
        
        //tags
        self.txtNombre.tag = 1
        self.txtCedula.tag = 2
        self.txtCorreoe.tag = 3
        self.txtCelular.tag = 4
        self.txtDireccionTrabajo.tag = 5
        
        //para la fecha de nacimiento
        let datePickerView : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        let now = NSDate()
        
        datePickerView.maximumDate = now
        
        self.txtFNacimiento.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), forControlEvents: .ValueChanged)
        
        //boton editar
        self.btnEditar.addTarget(self, action: #selector(actualizarDatos), forControlEvents: .TouchUpInside)
        
        //delegate del image picker y traer la galeria
        self.imagePicker.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.seleccionaImagen))
        self.imgUsuario.userInteractionEnabled = true
        self.imgUsuario.addGestureRecognizer(tap)
        
        //asignar accion para limpiar si el campo es modificado
        self.txtNombre.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCedula.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCorreoe.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCelular.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtDireccionTrabajo.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        //llena la forma
        self.llenaForma()

    }
    
    override func viewWillAppear(animated: Bool) {
        
        //imagen
        if (self.userDefaults.stringForKey(Constantes.JsonData.usuario.facebook_id) == ""){
            let dataDecoded:NSData = NSData(base64EncodedString: self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.foto)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
            
            let decodedimage:UIImage = UIImage(data: dataDecoded)!
            self.imgUsuario.image = decodedimage
        } else {
            let imgURL = NSURL(string: self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.foto)!)
            self.imgUsuario.kf_setImageWithURL(imgURL)
        }
        
        //imagen redonda
        
        self.imgUsuario.layer.cornerRadius = (self.imgUsuario.frame.size.height / 2 )
        self.imgUsuario.clipsToBounds = true
        self.imgUsuario.layer.masksToBounds = true
        self.imgUsuario.layer.borderColor = UIColor.whiteColor().CGColor
        self.imgUsuario.layer.borderWidth = 1
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //imagen redonda -- checar que se puede hacer para que no haga ese efecto de cambiar de forma
        self.imgUsuario.layer.cornerRadius = (self.imgUsuario.frame.size.height / 2 )
        self.imgUsuario.hidden = false
        
        //observers para mover la pantalla cuando el teclado aparece o se oculta
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        //Ocultar si esta abierto el teclado
        self.hideKeyboard()

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    //MARK: llenar la forma
    func llenaForma(){
        
        self.txtNombre.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.nombre)
        self.txtApellidos.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.apellido)
        self.txtCedula.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.cedula)
        self.txtCelular.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.celular)
        self.txtTelefono.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.telefono)
        self.txtFNacimiento.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.fecha_nacimiento)
        self.txtCorreoe.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.correo_personal)
        self.txtDireccionCasa.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.direccion_casa)
        self.txtDireccionTrabajo.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.direccion_trabajo)
        
        //los campos debajo de la imagen
        self.lblNombreCompleto.text = (self.txtNombre.text?.uppercaseString)! + " " + (self.txtApellidos.text?.uppercaseString)!
        self.lblCedula.text = "C.I. " + self.txtCedula.text!
        self.lblCorreoUsuario.text = self.txtCorreoe.text!
        
        self.usr_id = self.userDefaults.stringForKey(Constantes.JsonData.usuario.usuario_id)!
        
    }
    
    //MARK: Para ocultar teclado
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Dismisses the Keyboard by making the text field resign
        // first responder
        textField.resignFirstResponder()
        
        // returns false. Instead of adding a line break, the text
        // field resigns
        return false
    }
    
    /*
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.scrollContainer.endEditing(true)
        self.view.endEditing(true)
    }
 */
    //para ocultar teclado
    
    //para mover la pantalla cuando se muestra el teclado
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollContainer.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollContainer.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        var contentInset:UIEdgeInsets = UIEdgeInsetsZero
        contentInset.top += 64 //se respeta el offset que trae el constraint de container view respecto al top
        self.scrollContainer.contentInset = contentInset
    }
    //para mover la pantalla cuando se muestra el teclado
    
    //MARK: limpia el array errores
    func limpiaArrayError(){
        for dicCode in self.boolsError.keys{
            self.boolsError[dicCode] = false
        }
    }
    
    //MARK: Valida forma
    func esValidaLaForma()->Bool{
        var error = false
        limpiaArrayError()
        
        let xfunciones = funciones()
        let nombre = self.txtNombre.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (nombre == "" || nombre?.characters.count < 4){
            xfunciones.markInvalid(self.txtNombre)
            xfunciones.shakeView(self.txtNombre)
            if (!error) { error = true }
            self.boolsError["nombre"] = true
        }
        
        //pendiente tener el algoritmo de validacion
        let cedula = self.txtCedula.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if (cedula == ""){
            xfunciones.markInvalid(self.txtCedula)
            xfunciones.shakeView(self.txtCedula)
            self.boolsError["cedula"] = true
            if (!error) { error = true }
        }
        
        let correo = self.txtCorreoe.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (!xfunciones.isValidEmailAddress(correo!) || correo?.characters.count < 4){
            xfunciones.markInvalid(self.txtCorreoe)
            xfunciones.shakeView(self.txtCorreoe)
            self.boolsError["correo"] = true
            if (!error) { error = true }
        }
        
        let numcelular = self.txtCelular.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (numcelular == "" || numcelular?.characters.count < 4){
            xfunciones.markInvalid(self.txtCelular)
            xfunciones.shakeView(self.txtCelular)
            self.boolsError["celular"] = true
            if (!error) { error = true }
        }
        
        let dirTrabajo = self.txtDireccionTrabajo.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (dirTrabajo == "" || dirTrabajo?.characters.count < 4){
            xfunciones.markInvalid(self.txtDireccionTrabajo)
            xfunciones.shakeView(self.txtDireccionTrabajo)
            self.boolsError["dirTrabajo"] = true
            if (!error) { error = true }
        }
        
        if !error {
            self.datos_usuario = datosUsuario()
            //campos validados
            self.datos_usuario.nombre = nombre!
            self.datos_usuario.cedula = cedula!
            self.datos_usuario.correo = correo!
            self.datos_usuario.celular = numcelular!
            self.datos_usuario.direccion_trabajo = dirTrabajo!
            
            //campos no validados
            let apellidos = self.txtApellidos.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            self.datos_usuario.apellidos = apellidos!
            
            let telefono = self.txtTelefono.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            self.datos_usuario.telefono = telefono!
            
            let fnac = self.txtFNacimiento.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            self.datos_usuario.fecha_nac = fnac!
            
            let dir_casa = self.txtDireccionCasa.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            self.datos_usuario.lugar_residencia = dir_casa!
            
            self.datos_usuario.foto = self.imgUsuario.image
            
        }
        
        return !error
    }
    
    //MARK: para generar json
    func generaJsonDatosUsuario()->String{
        
        let xfunciones = funciones()
        
        let paraJson: [String: AnyObject] = [
            "metodo": "editarUsuario",
            "parametros": [
                [
                    "nombre":Constantes.JsonData.datosUsuario.nombre,
                    "valor": self.datos_usuario.nombre
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.apellido,
                    "valor": self.datos_usuario.apellidos
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.fecha_nacimiento,
                    "valor": self.datos_usuario.fecha_nac
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.correo_personal,
                    "valor": self.datos_usuario.correo
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.direccion_casa,
                    "valor": self.datos_usuario.lugar_residencia
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.direccion_trabajo,
                    "valor": self.datos_usuario.direccion_trabajo
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.telefono,
                    "valor": self.datos_usuario.telefono
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.celular,
                    "valor": self.datos_usuario.celular
                ],
                
                [
                    "nombre":Constantes.JsonData.datosUsuario.cedula,
                    "valor": self.datos_usuario.cedula
                ],
                [
                    "nombre": Constantes.JsonData.usuario.usuario_id,
                    "valor": self.usr_id
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.foto,
                    "valor": xfunciones.imgToBase64(self.datos_usuario.foto)
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            
            return jsonString
        } else {
            return ""
        }
        
    }
    
    
    //MARK: Accion para el boton editar
    func actualizarDatos(){
        if (self.esValidaLaForma()){
            
            
            let dtosJson = self.generaJsonDatosUsuario()
            
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                return
            }
            //preparar los datos para envio
         
             //loader de carga
             let alert = UIAlertController(title: nil, message: "Por favor espere...", preferredStyle: .Alert)
             
             alert.view.tintColor = UIColor.blackColor()
             let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(10, 5, 50, 50)) as UIActivityIndicatorView
             loadingIndicator.hidesWhenStopped = true
             loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
             loadingIndicator.startAnimating();
             
             alert.view.addSubview(loadingIndicator)
             presentViewController(alert, animated: true, completion: nil)
            //loader de carga
            
            //para el envio de datos
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
            
            //envio de datos
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                let httpResponse = response as? NSHTTPURLResponse
                
                if httpResponse?.statusCode != 200 {
                    print(httpResponse?.statusCode)
                }
                
                if ( error != nil ) {
                    print("Localized description error: \(error!.localizedDescription)")
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        alert.dismissViewControllerAnimated(true, completion: nil)
                    }
                    
                    let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar realizar el registro", preferredStyle: .Alert)
                    let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                        //No hacer nada por ahora
                    }
                    alertaError.addAction(okActionError)
                    self.presentViewController(alertaError, animated: true, completion: nil)
                    
                    
                } else {
                    do {
                        //deserializar los datos
                        let json = try Json.deserialize(data!)
                        let resultado = json["resultado"]?.stringValue ?? ""
                        
                        //quitar el loader
                        dispatch_async(dispatch_get_main_queue()){
                            alert.dismissViewControllerAnimated(true, completion: nil)
                        }
                        
                        
                        if (resultado == "error"){
                            
                            var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar realizar la actualización"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            self.presentViewController(alertaError, animated: true, completion: nil)
                            
                        }
                            
                        else if (resultado == "ok"){
                            
                            //actualizar datos de usuario
                            self.actualizarUserDefaults()
                            
                            var texto = json["texto"]?.stringValue ?? "Se han actualizado los datos correctamente"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaOK = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let alertaOKAccion = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaOK.addAction(alertaOKAccion)
                            self.presentViewController(alertaOK, animated: true, completion: nil)
                            
                        } else {
                            //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                            //enviar alerta de error
                            print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                        }
                        
                    } catch {
                        print("Json serialization failed with error: \(error)")
                    }
                    
                }
            } //hasta aqui el data task request
            
            task.resume()
            
            //hasta aca envio de datos
            
        }
    }
    
    //MARK: Actualizar datos user defaults
    func actualizarUserDefaults(){
        self.userDefaults.setObject(self.datos_usuario.nombre, forKey: Constantes.JsonData.datosUsuario.nombre)
        self.userDefaults.setObject(self.datos_usuario.apellidos, forKey: Constantes.JsonData.datosUsuario.apellido)
        self.userDefaults.setObject(self.datos_usuario.cedula, forKey: Constantes.JsonData.datosUsuario.cedula)
        self.userDefaults.setObject(self.datos_usuario.correo, forKey: Constantes.JsonData.datosUsuario.correo_personal)
        self.userDefaults.setObject(self.datos_usuario.celular, forKey: Constantes.JsonData.datosUsuario.celular)
        self.userDefaults.setObject(self.datos_usuario.direccion_trabajo, forKey: Constantes.JsonData.datosUsuario.direccion_trabajo)
        self.userDefaults.setObject(self.datos_usuario.telefono, forKey: Constantes.JsonData.datosUsuario.telefono)
        self.userDefaults.setObject(self.datos_usuario.fecha_nac, forKey: Constantes.JsonData.datosUsuario.fecha_nacimiento)
        self.userDefaults.setObject(self.datos_usuario.lugar_residencia, forKey: Constantes.JsonData.datosUsuario.direccion_casa)
        let xfunciones = funciones()
        self.userDefaults.setObject(xfunciones.imgToBase64(self.datos_usuario.foto), forKey: Constantes.JsonData.datosUsuario.foto)
    }
    
    
    //MARK: Para el datepicker select
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.txtFNacimiento.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    //para el date picker select
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    //MARK: Para image picker
    func seleccionaImagen(sender: UITapGestureRecognizer){
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .PhotoLibrary
        self.imagePicker.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.imagePicker.navigationBar.tintColor = UIColor.whiteColor()
        let attrs = [
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        self.navigationController?.navigationBar.titleTextAttributes = attrs
        
        presentViewController(self.imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let imagenSelec = info[UIImagePickerControllerOriginalImage] as? UIImage {
            dispatch_async(dispatch_get_main_queue(), {
                //let nuevaImg = self.ResizeImage(imagenSelec, targetSize: CGSizeMake(300, 300))
                //let nuevaImg = self.imageWithImage(imagenSelec, scaledToWidth: 300)
                let nuevaImg = self.scaleAndCropImage(imagenSelec, toSize: CGSizeMake(300, 300))
                self.imgUsuario.image = nuevaImg
                self.imgUsuario.setNeedsDisplay()
            })
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //PAra resize de la imagen
    func scaleAndCropImage(image:UIImage, toSize size: CGSize) -> UIImage {
        // Sanity check; make sure the image isn't already sized.
        if CGSizeEqualToSize(image.size, size) {
            return image
        }
        
        let widthFactor = size.width / image.size.width
        let heightFactor = size.height / image.size.height
        var scaleFactor: CGFloat = 0.0
        
        scaleFactor = heightFactor
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }
        
        var thumbnailOrigin = CGPointZero
        let scaledWidth  = image.size.width * scaleFactor
        let scaledHeight = image.size.height * scaleFactor
        
        if widthFactor > heightFactor {
            thumbnailOrigin.y = (size.height - scaledHeight) / 2.0
        }
            
        else if widthFactor < heightFactor {
            thumbnailOrigin.x = (size.width - scaledWidth) / 2.0
        }
        
        var thumbnailRect = CGRectZero
        thumbnailRect.origin = thumbnailOrigin
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.drawInRect(thumbnailRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.drawInRect(CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func textFieldShouldBeginEditing(xtextField: UITextField) -> Bool {
        
        let tiempo : NSTimeInterval = 2
        
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["nombre"]) != nil) && (self.boolsError["nombre"] == true){
                let tte_nombre = AMPopTip()
                tte_nombre.shouldDismissOnTap = true
                tte_nombre.shouldDismissOnTapOutside = true
                tte_nombre.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtNombre.frame, duration: tiempo)
            }
            break
        case 2:
            if ((self.boolsError["cedula"]) != nil) && (self.boolsError["cedula"] == true){
                let tte_ced = AMPopTip()
                tte_ced.shouldDismissOnTap = true
                tte_ced.shouldDismissOnTapOutside = true
                tte_ced.showText("No puede ingresar caracteres en el número", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCedula.frame, duration: tiempo)
            }
            break
        case 3:
            if ((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true){
                let tte_ce = AMPopTip()
                tte_ce.shouldDismissOnTap = true
                tte_ce.shouldDismissOnTapOutside = true
                tte_ce.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCorreoe.frame, duration: tiempo)
            }
            break
        case 4:
            if ((self.boolsError["celular"]) != nil) && (self.boolsError["celular"] == true){
                let tte_cel = AMPopTip()
                tte_cel.shouldDismissOnTap = true
                tte_cel.shouldDismissOnTapOutside = true
                tte_cel.showText("Ingrese información", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCelular.frame, duration: tiempo)
            }
            break
        case 5:
            if ((self.boolsError["dirTrabajo"]) != nil) && (self.boolsError["dirTrabajo"] == true){
                let tte_fn = AMPopTip()
                tte_fn.shouldDismissOnTap = true
                tte_fn.shouldDismissOnTapOutside = true
                tte_fn.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtDireccionTrabajo.frame, duration: tiempo)
            }
            break
            
        default:
            break
        }
        return true
    }
    
    //MARK: para los on change de los textfields
    func textFieldDidChange(xtextField: UITextField) {
        let xfunciones = funciones()
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["nombre"]) != nil) && (self.boolsError["nombre"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["nombre"] = false
            }
            break
        case 2:
            if ((self.boolsError["cedula"]) != nil) && (self.boolsError["cedula"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["cedula"] = false
            }
            break
            
        case 3:
            if ( (((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["correo"] = false
            }
            break
        case 4:
            if ( (((self.boolsError["celular"]) != nil) && (self.boolsError["celular"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["celular"] = false
            }
            break
        case 5:
            if ( (((self.boolsError["dirTrabajo"]) != nil) && (self.boolsError["dirTrabajo"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["dirTrabajo"] = false
            }
            break
            
        default:
            break
        }
        
    }


}
