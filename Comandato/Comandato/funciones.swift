//
//  funciones.swift
//  Comandato
//
//  Created by Leonel Sánchez on 17/02/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import Foundation
import PureJsonSerializer
import SwiftyJSON

class funciones {
    
    let userDefaults = NSUserDefaults.standardUserDefaults() 
    
    func guardaDatosUsuario(json: JSON){
        self.BorraDatosUsuario()
        
        //Marcar como usuario logueado
        self.userDefaults.setBool(true, forKey: Constantes.JsonData.base.usrLogueado)
        
        //numero
        let basenum = json["numero"].intValue ?? 0
        self.userDefaults.setInteger(basenum, forKey: Constantes.JsonData.base.baseNumero)
        
        //lista->usuario
        var dato = json["lista"]["usuario"]["email"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.usuario.email)
        
        dato = json["lista"]["usuario"]["estado_id"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.usuario.estado_id)
        
        let facebookId = json["lista"]["usuario"]["facebook_id"].stringValue ?? ""
        self.userDefaults.setObject(facebookId, forKey: Constantes.JsonData.usuario.facebook_id)
        
        
        dato = json["lista"]["usuario"]["fecha_creacion"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.usuario.fecha_creacion)
        
        
        dato = json["lista"]["usuario"]["password"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.usuario.password)
        
        
        let rememberToken = json["lista"]["usuario"]["remember_token"].stringValue ?? ""
        self.userDefaults.setObject(rememberToken, forKey: Constantes.JsonData.usuario.remember_token)
        
        dato = json["lista"]["usuario"]["tipo_usuario_id"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.usuario.tipo_usuario_id)
        
        
        dato = json["lista"]["usuario"]["usuario_id"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.usuario.usuario_id)
        
        
        //lista->datos
        dato = json["lista"]["datos"]["apellido"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.apellido)
        
        
        dato = json["lista"]["datos"]["cedula"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.cedula)
        
        
        dato = json["lista"]["datos"]["celular"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.celular)
        
        
        dato = json["lista"]["datos"]["correo_personal"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.correo_personal)
        
        
        dato = json["lista"]["datos"]["dato_personal_id"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.dato_personal_id)
        
        
        dato = json["lista"]["datos"]["direccion_casa"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.direccion_casa)
        
        
        dato = json["lista"]["datos"]["direccion_trabajo"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.direccion_trabajo)
        
        
        dato = json["lista"]["datos"]["fecha_creacion"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.fecha_creacion)
        
        
        dato = json["lista"]["datos"]["fecha_modificacion"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.fecha_modificacion)
        
        
        dato = json["lista"]["datos"]["fecha_nacimiento"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.fecha_nacimiento)
        
        
        dato = json["lista"]["datos"]["foto"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.foto)
        
        
        dato = json["lista"]["datos"]["nombre"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.nombre)
        
        
        dato = json["lista"]["datos"]["telefono"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosUsuario.telefono)
        
        
        //lista->intentos
        dato = json["lista"]["intentos"][0]["intentos"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.base.intentos)
        
        
        //lista->datosComandato
        let cupo = json["lista"]["datosComandato"]["cupo"].intValue ?? 0
        self.userDefaults.setInteger(cupo, forKey: Constantes.JsonData.datosComandato.comandato_cupo)
        
        
        dato = json["lista"]["datosComandato"]["cliente"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.datosComandato.comandato_cliente)
        
        
        //lista->nivelUsuario
        dato = json["lista"]["nivelUsuario"][0]["nivel_id"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.nivelUsuario.nivel_id)
        
        
        dato = json["lista"]["nivelUsuario"][0]["nivel_usuario_id"].stringValue ?? ""
        self.userDefaults.setObject(dato, forKey: Constantes.JsonData.nivelUsuario.nivel_usuario_id)
        
        
        //lista->totalPuntos
        
        let arrayPuntos = json["lista"]["totalPuntos"].arrayValue
        
        self.userDefaults.setInteger(arrayPuntos.count, forKey: Constantes.JsonData.base.cantMonedas)
        
        for index in 0...arrayPuntos.count{
            
            let nombreMoneda = json["lista"]["totalPuntos"][index]["nombre"].stringValue ?? ""
            
            self.userDefaults.setObject(nombreMoneda, forKey: "\(nombreMoneda)_nombre")
            
            
            let cantidad_padre = json["lista"]["totalPuntos"][index]["cantidad_padre"].stringValue ?? ""
            
            self.userDefaults.setObject(cantidad_padre, forKey: "\(nombreMoneda)_\(Constantes.JsonData.totalPuntos.cantidad_padre)")
            
            dato = json["lista"]["totalPuntos"][index]["usuario_id"].stringValue ?? ""
            self.userDefaults.setObject(dato, forKey: "\(nombreMoneda)_\(Constantes.JsonData.totalPuntos.usuario_id)")
            
            
            dato = json["lista"]["totalPuntos"][index]["punto_id"].stringValue ?? ""
            self.userDefaults.setObject(dato, forKey: "\(nombreMoneda)_\(Constantes.JsonData.totalPuntos.punto_id)")
            
            
            let puntoPadreId = json["lista"]["totalPuntos"][index]["punto_padre_id"].stringValue ?? ""
            self.userDefaults.setObject(puntoPadreId, forKey: "\(nombreMoneda)_\(Constantes.JsonData.totalPuntos.punto_padre_id)")
            
            dato = json["lista"]["totalPuntos"][index]["total"].stringValue ?? ""
            self.userDefaults.setObject(dato, forKey: "\(nombreMoneda)_\(Constantes.JsonData.totalPuntos.total)")
            
            
            dato = json["lista"]["totalPuntos"][index]["url_imagen"].stringValue ?? ""
            self.userDefaults.setObject(dato, forKey: "\(nombreMoneda)_\(Constantes.JsonData.totalPuntos.url_imagen)")
            
            
        }
        
        
        //agregar funcion sincronizar si utiliza una version menor a 8 en ios
        //verificar que sea una version menor a 8
        self.userDefaults.synchronize()
    } //fin guardar usuario
    
    func BorraDatosUsuario(){
        //quitar datos al user defaults por si tuviera, para hacer una asignacion nueva
        let appDomain = NSBundle.mainBundle().bundleIdentifier!
        self.userDefaults.removePersistentDomainForName(appDomain)
    } // fin borrar datos usuario
    
    func isValidEmailAddress(text: String) -> Bool {
        guard !text.hasPrefix("mailto:") else { return false }
        guard let emailDetector = try? NSDataDetector(types: NSTextCheckingType.Link.rawValue) else { return false }
        let matches = emailDetector.matchesInString(text, options: NSMatchingOptions.Anchored, range: NSRange(location: 0, length: text.characters.count))
        guard matches.count == 1 else { return false }
        return matches[0].URL?.scheme == "mailto"
    }
    
    func shakeView(shakeView: UIView) {
        let shake = CABasicAnimation(keyPath: "position")
        let xDelta = CGFloat(5)
        shake.duration = 0.10
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let from_point = CGPointMake(shakeView.center.x - xDelta, shakeView.center.y)
        let from_value = NSValue(CGPoint: from_point)
        
        let to_point = CGPointMake(shakeView.center.x + xDelta, shakeView.center.y)
        let to_value = NSValue(CGPoint: to_point)
        
        shake.fromValue = from_value
        shake.toValue = to_value
        shake.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        shakeView.layer.addAnimation(shake, forKey: "position")
    }
    
    func markInvalid(xtextfield:UIView){
        xtextfield.layer.borderWidth = 1
        xtextfield.layer.borderColor = UIColor.redColor().CGColor
        xtextfield.layer.cornerRadius = 5
        xtextfield.clipsToBounds = true
    }
    
    func markValid(xtextfield: UIView){
        xtextfield.layer.borderColor = UIColor.clearColor().CGColor
    }
    
    func imgToBase64(laimagen:UIImage)->String{
        let imageData = UIImageJPEGRepresentation(laimagen, 1)
        let base64String = imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return base64String
    }
    
}//fin de clase