//
//  PremiosObtenidosVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 30/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import Kingfisher
import PureJsonSerializer

class PremiosObtenidosVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var PremiosObtenidosTable: UITableView!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    //arreglo de premios
    var itemsPremiosObtenidos = [premioObtenido]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.PremiosObtenidosTable.dataSource = self
        self.PremiosObtenidosTable.delegate = self
        
        //ocultar celdas vacias
        self.PremiosObtenidosTable.tableFooterView = UIView(frame: CGRectZero)
        
        self.cargaDatos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Carga datos del WS
    func cargaDatos(){
        var dtosJson = "{\"metodo\":\"premiosObtenidosXUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        print(dtosJson)
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                }
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los premios promocion", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    print(json)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                    }
                    if (resultado == "error"){
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar obtener los premios promocion"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        print(json["lista"])
                        self.itemsPremiosObtenidos.removeAll()
                        if (!(json["lista"]?.isNull)!){
                            for item in (json["lista"]?.arrayValue)! {
                                let unPremio = premioObtenido()
                                unPremio.premio_id = (item["premio_id"]?.stringValue)!
                                unPremio.nombre = (item["nombre"]?.stringValue)!.uppercaseString
                                unPremio.cantidad = (item["cantidad"]?.stringValue)! //cantidad de monedas
                                unPremio.punto_id = (item["punto_id"]?.stringValue)!
                                if (item["fecha_inicio"]?.stringValue)! != nil {
                                    unPremio.fecha_inicio = (item["fecha_inicio"]?.stringValue)!
                                } else {
                                    unPremio.fecha_inicio = ""
                                }
                                
                                if (item["fecha_fin"]?.stringValue)! != nil {
                                    unPremio.fecha_fin = (item["fecha_fin"]?.stringValue)!
                                } else {
                                    unPremio.fecha_fin = ""
                                }
                                
                                unPremio.punto_id = (item["punto_id"]?.stringValue)!
                                unPremio.stock = (item["stock"]?.stringValue)!
                                unPremio.nivel_id = (item["nivel_id"]?.stringValue)!
                                unPremio.url_premio = (item["url_premio"]?.stringValue)! //imagen del premio
                                
                                if (item["nota"]?.stringValue)! != nil {
                                    unPremio.nota = (item["nota"]?.stringValue)!
                                } else {
                                    unPremio.nota = ""
                                }
                                
                                unPremio.estado_id = (item["estado_id"]?.stringValue)!
                                unPremio.estado = (item["estado"]?.stringValue)!
                                unPremio.fecha = (item["fecha"]?.stringValue)!
                                //unPremio.punto = (item["punto"]?.stringValue)!
                                unPremio.url_imagen = (item["url_imagen"]?.stringValue)! //imagen de la moneda
                                //unPremio.usuario_id = (item["usuario_id"]?.stringValue)!
                                //unPremio.cantidad_promocion = (item["cantidad_promocion"]?.stringValue)!
                                //unPremio.promocion_id = (item["promocion_id"]?.stringValue)!
                                //unPremio.valido_hasta = (item["valido_hasta"]?.stringValue)!
                                
                                self.itemsPremiosObtenidos.append(unPremio)
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.stopActivityIndicator()
                                self.PremiosObtenidosTable.reloadData()
                            })
                        } else {
                            NSLog("se consulto correctamente los premios obtenidos, pero la lista llego null")
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.itemsPremiosObtenidos.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("celdaPremiosObtenidos", forIndexPath: indexPath) as! celdaPremiosObtenidosTVC
        
        let itemPremio = self.itemsPremiosObtenidos[indexPath.row]
        
        var imgURL = NSURL(string: itemPremio.url_premio)
        
        cell.imgPremio.kf_setImageWithURL(imgURL)
        
        imgURL = NSURL(string: itemPremio.url_imagen)
        cell.imgMonedas.kf_setImageWithURL(imgURL)
        
        let fechastring = itemPremio.fecha
        let arrayfecha = fechastring.componentsSeparatedByString(" ")
        let fecha: String = arrayfecha[0]
    
        cell.lblPremio.text = itemPremio.nombre
        cell.lblFecha.text = itemPremio.valido_hasta
        cell.lblNivel.text = "\(fecha)   Nivel: \(itemPremio.nivel_id)"
        cell.lblCantMonedas.text = itemPremio.cantidad
        cell.btnRetirarPremio.setTitle(itemPremio.estado, forState: UIControlState.Normal)
        
        //espacio a la celda
        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height-10))
        whiteRoundedView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 0.7])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
        //espacio a las celdas
        
        cell.selectionStyle = .None
        
        return cell
    }

    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


class premioObtenido{
    var premio_id = ""
    var nombre = ""
    var cantidad = ""
    var punto_id = ""
    var fecha_inicio = ""
    var fecha_fin = ""
    var fecha_creacion = ""
    var stock = ""
    var nivel_id = ""
    var url_premio = ""
    var nota = ""
    var estado_id = ""
    var punto = ""
    var url_imagen = ""
    var usuario_id = ""
    var cantidad_promocion = ""
    var promocion_id = ""
    var valido_hasta = ""
    var estado = ""
    var fecha = ""
}
