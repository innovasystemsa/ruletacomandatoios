//
//  PremiosTableView.swift
//  Comandato
//
//  Created by Leonel Sánchez on 27/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class PremiosPageView: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
   
    private var pages: [UIViewController]!
    
    var indice = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.delegate = self
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let backImg = UIImageView(frame: UIScreen.mainScreen().bounds)
        backImg.image = UIImage(named: "backDragon")
        self.view.insertSubview(backImg, atIndex: 0)
        
        self.pages = [
            self.storyboard!.instantiateViewControllerWithIdentifier("PremiosDispVC"),
            self.storyboard!.instantiateViewControllerWithIdentifier("PremiosObtenidosVC"),
            self.storyboard!.instantiateViewControllerWithIdentifier("PremiosPromoVC")
        ]
        
        let startingViewController = self.pages.first! as UIViewController
        self.setViewControllers([startingViewController], direction: .Forward, animated: false, completion: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.cambiarPagina(_:)), name: "cambiaPagina", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("cambiarMarcador", object: nil, userInfo: ["pagina":"0"])
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        self.indice = (self.pages as NSArray).indexOfObject(viewController)
        
        // if currently displaying last view controller, return nil to indicate that there is no next view controller
        return (self.indice == self.pages.count - 1 ? nil : self.pages[self.indice + 1])
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        self.indice = (self.pages as NSArray).indexOfObject(viewController)
        
        // if currently displaying first view controller, return nil to indicate that there is no previous view controller
        return (self.indice == 0 ? nil : self.pages[self.indice - 1])
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        guard completed else { return }
        
        let elTag = pageViewController.viewControllers!.first!.view.tag
        
        NSNotificationCenter.defaultCenter().postNotificationName("cambiarMarcador", object: nil, userInfo: ["pagina":"\(elTag)"])
        
    }
    
    func cambiarPagina(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String, String!>
        if let pagina = userInfo["pagina"] {
            print(pagina)
            slideToPage(Int(pagina)!, completion: nil)
        }
    }
    
    func slideToPage(index: Int, completion: (() -> Void)?) {
        let tempIndex = self.indice
        if self.indice < index {
            for var i = tempIndex+1; i <= index; i += 1 {
                self.setViewControllers([self.pages[i]], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: {[weak self] (complete: Bool) -> Void in
                    if (complete) {
                        self?.indice = i-1
                        completion?()
                    }
                    })
            }
        }
        else if self.indice > index {
            for var i = tempIndex - 1; i >= index; i -= 1 {
                self.setViewControllers([self.pages[i]], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: {[weak self] (complete: Bool) -> Void in
                    if complete {
                        self?.indice = i+1
                        completion?()
                    }
                    })
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
