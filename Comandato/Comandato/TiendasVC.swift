//
//  TiendasVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 19/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import Foundation
//import PureJsonSerializer
import SwiftyJSON
import MapKit


class TiendasVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var txtPickTienda: UITextField!
    @IBOutlet weak var tiendasContainer: UICollectionView!
    
    var dic_ciudades = [String: String]()
    var nombres_ciudades = [String]()
    
    var nombreTiendas = [Tienda]()
    
    let reuseIdentifier = "celdaTienda"
    
    let opcionpicker : UIPickerView = UIPickerView()
    
    var tiendaSeleccionada = ""
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        

        
        //picker para las opciones, eventualmente se cargaran por webservice, por ahora fijo
        
        self.opcionpicker.delegate = self
        
        //evaluar si se necesita loader
        
        //cargar datos del picker
        self.cargaCiudades()
        
        //configuracion de controles
        self.txtPickTienda.layer.borderWidth = 0.5
        self.txtPickTienda.inputView = self.opcionpicker
        self.txtPickTienda.tintColor = UIColor.clearColor()
        self.txtPickTienda.delegate = self
        
        //icono derecho
        self.txtPickTienda.rightViewMode = UITextFieldViewMode.Always
        let imgViewFlecha = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let imgFlecha = UIImage(named: "imgFlechaAbajo")
        
        imgViewFlecha.image =  imgFlecha
        self.txtPickTienda.rightView = imgViewFlecha
        
        self.tiendasContainer.delegate = self
        self.tiendasContainer.dataSource = self
        self.tiendasContainer.backgroundColor = UIColor.whiteColor()
        
        //botones para picker view
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 100/255, blue: 238/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.donePicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(self.donePicker(_:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        self.txtPickTienda.inputAccessoryView = toolBar
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //carga ciudades para picker
    func cargaCiudades(){
        self.startActivityIndicator()
        let dtosJson = "{\"metodo\":\"todosCiudad\"}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener las ciudades comandato", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = JSON(data: data!)
                    let resultado = json["resultado"].stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        var texto = json["texto"].stringValue ?? "Ocurrio un error al intentar obtener las ciudades comandato"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        
                        //quitar el loader
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.nombres_ciudades.removeAll()
                        if ((json["lista"].exists())){
                            for item in (json["lista"].arrayValue) {
                                let id = item["ciudad_id"].stringValue
                                let nombre = item["nombre"].stringValue
                                self.dic_ciudades[nombre] = id
                                self.nombres_ciudades.append(nombre)
                            }
                            
                            dispatch_async(dispatch_get_main_queue()){
                                self.stopActivityIndicator()
                                self.txtPickTienda.text = self.nombres_ciudades[0]
                                self.cargaTiendasCiudad(Int(self.dic_ciudades[self.txtPickTienda.text!]!)!)
                            }
                        }
                        else {
                            NSLog("Cargando ciudades la lista llego null")
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                }
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    //MARK: Carga tiendas por ciudad
    func cargaTiendasCiudad(id_ciudad: Int){
        self.startActivityIndicator()
        var dtosJson = "{\"metodo\":\"tiendasXCiudad\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"ciudad_id\","
        dtosJson += "\"valor\": \(id_ciudad)"
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener las tiendas comandato", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = JSON(data: data!)
                    let resultado = json["resultado"].stringValue ?? ""
                    
                    
                    if (resultado == "error"){
                        
                        var texto = json["texto"].stringValue ?? "Ocurrio un error al intentar obtener las tiendas comandato"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.nombreTiendas.removeAll()
                        if ((json["lista"].exists())){
                            for item in (json["lista"].arrayValue) {
                                let laTienda : Tienda = Tienda()
                                laTienda.tienda_id = (item["tienda_id"].stringValue)
                                laTienda.ciudad_id = (item["ciudad_id"].stringValue)
                                laTienda.title = (item["title"].stringValue)
                                laTienda.direccion = (item["direccion"].stringValue)
                                laTienda.telefono = (item["telefono"].stringValue)
                                laTienda.horario = (item["horario"].stringValue)
                                laTienda.lat = (item["lat"].stringValue)
                                laTienda.lng = (item["lng"].stringValue)
                                laTienda.zoom = (item["zoom"].stringValue)
                                laTienda.estado_id = (item["estado_id"].stringValue)
                                self.nombreTiendas.append(laTienda)
                            }
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.tiendasContainer.reloadData()
                                    self.stopActivityIndicator()
                                })
                            })
                        } else {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.tiendasContainer.reloadData()
                                    self.stopActivityIndicator()
                                })
                            })
                            NSLog("Cargando tiendas, la lista llego null")
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.nombreTiendas.count
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.reuseIdentifier, forIndexPath: indexPath) as! celdaTiendas
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.lblNombreTienda.text = self.nombreTiendas[indexPath.item].title
        cell.lblDireccion.text = self.nombreTiendas[indexPath.item].direccion
        
        cell.backgroundColor = UIColor.whiteColor() // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.abreGMaps(self.nombreTiendas[indexPath.item])
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let ancho = self.tiendasContainer.frame.width / 2.1
        return CGSizeMake(ancho, 225)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    //MARK: Abre GMAPS
    func abreGMaps(xtienda: Tienda){
        
        let arr_nombreTienda = xtienda.title.characters.split{$0 == " "}.map(String.init)
        var stringNombre = ""
        for item in arr_nombreTienda {
            stringNombre += "+\(item)"
        }
        let stringGMaps = "comgooglemaps://?q=@Comandato\(stringNombre)&center=\(xtienda.lat),\(xtienda.lng)&zoom=\(xtienda.zoom)"
        
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
            UIApplication.sharedApplication().openURL(NSURL(string: stringGMaps)!)
        
        } else {
            openMapForPlace(xtienda.lat, longitud: xtienda.lng, nombre: xtienda.title)
        }
    
    }
    
    func openMapForPlace(latitud: NSString, longitud: NSString, nombre: String) {
        
        let lat1 : NSString = latitud
        let lng1 : NSString = longitud
        
        let latitude:CLLocationDegrees =  lat1.doubleValue
        let longitude:CLLocationDegrees =  lng1.doubleValue
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(nombre)"
        mapItem.openInMapsWithLaunchOptions(options)
        
    }
    
    //MARK: Metodos para el pickerview
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.nombres_ciudades.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.nombres_ciudades[row]
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = self.nombres_ciudades[row]
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        //pickerLabel.font = UIFont(name: "Arial", size: 15) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tiendaSeleccionada = self.nombres_ciudades[row]
    }
    
    //hasta aca metodos para pickerview
    
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func donePicker (sender: UIBarButtonItem){
        if (sender.title! == "OK"){
            self.txtPickTienda.text = self.tiendaSeleccionada
            //aqui cargar las tiendas
            self.cargaTiendasCiudad(Int(self.dic_ciudades[self.tiendaSeleccionada]!)!)
        }
        
        self.txtPickTienda.endEditing(true)
        
        
    }
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

} //fin de clase

class Tienda {
    var tienda_id = ""
    var ciudad_id = ""
    var title = ""
    var direccion = ""
    var telefono = ""
    var horario = ""
    var lat = ""
    var lng = ""
    var zoom = ""
    var estado_id = ""
}
