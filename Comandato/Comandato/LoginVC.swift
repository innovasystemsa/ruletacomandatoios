//
//  ViewController.swift
//  Comandato
//
//  Created by Leonel Sánchez on 10/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import Kingfisher
import Foundation
//import PureJsonSerializer
import AMPopTip
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class LoginVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var navigationBarItem: UINavigationItem!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtContra: UITextField!
    @IBOutlet weak var btnIngresar: UIButton!
    @IBOutlet weak var btnIngresarFB: UIButton!
    @IBOutlet weak var btnCrearCuenta: UIButton!
    @IBOutlet weak var btnOlvideContra: UIButton!
    @IBOutlet weak var scrollViewContainer: UIScrollView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var stackTop: UIStackView!
    
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    //array para los errores
    var boolsError: [String:Bool] = ["correo":false,"contrasenia":false]
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

    var correo_usr = ""
    var password_usr = ""
    
    //para los datos de FB
    var creandoCtaFB = false
    var nombreFB = ""
    var emailFB = ""
    var idFB = ""
    var fotoFB = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //poner el icono en el campo de contraseña
        self.txtContra.rightViewMode = UITextFieldViewMode.Always
        let txtImgViewR = UIButton(type: UIButtonType.Custom) as UIButton
        txtImgViewR.frame = CGRect(x: 0, y: 0, width: 20, height: 10)
        let rightImg = UIImage(named: "imgOjo")
        txtImgViewR.setImage(rightImg, forState: .Normal)
        var insets : UIEdgeInsets = txtImgViewR.contentEdgeInsets
        insets.right += 5
        txtImgViewR.contentEdgeInsets = insets
        txtImgViewR.addTarget(self, action: #selector(self.muestraOcultaPass), forControlEvents: .TouchUpInside)
        self.txtContra.rightView = txtImgViewR
        
        self.txtCorreo.delegate = self
        self.txtContra.delegate = self
        
        //setear tags
        self.txtCorreo.tag = 1
        self.txtContra.tag = 2
        
        //Para que los botones de la barra de navegacion se vean en contraste con el fondo
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        //agregar borde al boton de facebook
        self.btnIngresarFB.layer.borderWidth = 0.5
        self.btnIngresarFB.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.btnIngresarFB.addTarget(self, action: #selector(self.FBButtonClicked), forControlEvents: .TouchUpInside)
        
        //asignar accion on change para textfield
        self.txtCorreo.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtContra.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        //btn crear cuenta
        self.btnCrearCuenta.addTarget(self, action: #selector(self.segueCrearCuenta), forControlEvents: .TouchUpInside)
        
        
    }
    
    func muestraOcultaPass(){
        if (self.txtContra.secureTextEntry){
            self.txtContra.secureTextEntry = false
        } else {
            self.txtContra.secureTextEntry = true
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
       // bannerImg?.scaleToSize(CGSize(width: <#T##CGFloat#>, height: <#T##CGFloat#>))
        let bannerImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 179, height: 13))
        bannerImgView.image = bannerImg
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        
        //observers para mover la pantalla cuando el teclado aparece o se oculta
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        //Ocultar si esta abierto el teclado
        self.hideKeyboard()
        
        self.navigationItem.titleView = bannerImgView
        
        //el padding para centrar el logo
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
        
        self.btnIngresar.addTarget(self, action: #selector(ingresarApp), forControlEvents: .TouchUpInside)
        
        self.creandoCtaFB = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    //MARK: Para ocultar teclado
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Dismisses the Keyboard by making the text field resign
        // first responder
        textField.resignFirstResponder()
        
        // returns false. Instead of adding a line break, the text
        // field resigns
        return false
    }
    
    /*
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.viewContainer.endEditing(true)
        self.view.endEditing(true)
    }
 */
    //para ocultar teclado
    
    
    //para mover la pantalla cuando se muestra el teclado
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollViewContainer.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollViewContainer.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollViewContainer.contentInset = contentInset
    }
    //para mover la pantalla cuando se muestra el teclado
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    //MARK: funciones Generales
    func limpiaArrayError(){
        for dicCode in self.boolsError.keys{
            self.boolsError[dicCode] = false
        }
    }
    
    func segueCrearCuenta(){
        self.performSegueWithIdentifier("LoginToCrearCuentaSegue", sender: self)
    }
    
    //MARK: VALIDA FORMA
    func esValidaLaForma()->Bool{
        //cedula, correo, contra y confcontra
        var error = false
        limpiaArrayError()
        
        let xfunciones = funciones()
        
        let correo = self.txtCorreo.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (!xfunciones.isValidEmailAddress(correo!) || correo?.characters.count < 4){
            xfunciones.markInvalid(self.txtCorreo)
            xfunciones.shakeView(self.txtCorreo)
            self.boolsError["correo"] = true
            if (!error) { error = true }
        }
        
        let contrasenia = self.txtContra.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (contrasenia?.characters.count < 4){
            xfunciones.markInvalid(self.txtContra)
            xfunciones.shakeView(self.txtContra)
            self.boolsError["contrasenia"] = true
            if (!error) { error = true }
            
        }

        
        if (!error){
            //datos validados sin error
            //validar la forma
            self.correo_usr = correo!
            self.password_usr = contrasenia!
        }
        
        return !error
    }
    
    //MARK: get json
    func jsonDatos()->String{
        let paraJson: [String: AnyObject] = [
            "metodo": "consultarUsuario",
            "parametros": [
                [
                    "nombre": Constantes.JsonData.usuario.email,
                    "valor": self.correo_usr
                ],
                [
                    "nombre":Constantes.JsonData.usuario.password,
                    "valor": self.password_usr
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            return jsonString
        } else {
            return ""
        }
    }
    
    
    //MARK: ingresar a la app
    //ingresar
    func ingresarApp(){
        if (self.esValidaLaForma()){
            self.txtContra.endEditing(true)
            self.txtCorreo.endEditing(true)
            //loader de carga
            let alert = UIAlertController(title: nil, message: "Por favor espere...", preferredStyle: .Alert)
            
            alert.view.tintColor = UIColor.blackColor()
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(10, 5, 50, 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            presentViewController(alert, animated: true, completion: nil)
            
            //obtener el json de los datos
            let JsonDatos = self.jsonDatos()
            
            let xfunciones = funciones()
            
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = JsonDatos.dataUsingEncoding(NSUTF8StringEncoding)
            
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print("A ocurrido un error: \(error)")
                    return
                }
                
                do {
                    //deserializar los datos
                    let json = JSON(data: data!)
                    let resultado = json["resultado"].stringValue ?? ""
                    
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        alert.dismissViewControllerAnimated(false, completion: nil)
                    }
                    
                    if (resultado == "error"){
                        
                        //quitar datos al user defaults por si tuviera
                        xfunciones.BorraDatosUsuario()
                        
                        let texto = json["texto"].stringValue ?? "Nombre de usuario o contraseña incorrecta"
                        let tte = AMPopTip()
                        tte.shouldDismissOnTap = true
                        tte.shouldDismissOnTapOutside = true
                        tte.arrowSize = CGSizeMake(0, 0)
                        
                        dispatch_async(dispatch_get_main_queue()){
                            tte.showText(texto, direction: .Up, maxWidth: 250, inView: self.stackTop, fromFrame: self.btnIngresar.frame, duration: 3)
                        }
                    }
                        
                    else if (resultado == "ok"){
                        //guardar datos de usuario
                        xfunciones.guardaDatosUsuario(json)
                        NSOperationQueue.mainQueue().addOperationWithBlock {
                            self.performSegueWithIdentifier("LoginToPrincipalSegue", sender: self)
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                    }
                    
                    
                }
            }
            
            task.resume()
            
        }
     
    }
    
    //MARK: Para mostrar tooltips de error
    func textFieldShouldBeginEditing(xtextField: UITextField) -> Bool {
        let tiempo : NSTimeInterval = 2

        switch xtextField.tag {
        case 1:
            if ((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true){
                let tte_ce = AMPopTip()
                tte_ce.shouldDismissOnTap = true
                tte_ce.shouldDismissOnTapOutside = true
                tte_ce.showText("Campo obligatorio", direction: .Up, maxWidth: 200, inView: self.stackTop, fromFrame: self.txtCorreo.frame, duration: tiempo)
            }
            break
        case 2:
            if ((self.boolsError["contrasenia"]) != nil) && (self.boolsError["contrasenia"] == true){
                let tte_contra = AMPopTip()
                tte_contra.shouldDismissOnTap = true
                tte_contra.shouldDismissOnTapOutside = true
                tte_contra.showText("Campo obligatorio", direction: .Up, maxWidth: 200, inView: self.stackTop, fromFrame: self.txtContra.frame, duration: tiempo)
            }
            break
        default:
            break
        }
        return true
    }
    
    //MARK: para los on change de los textfields
    func textFieldDidChange(xtextField: UITextField) {
        let xfunciones = funciones()
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["correo"] = false
            }
            break
        case 2:
            if ((self.boolsError["contrasenia"]) != nil) && (self.boolsError["contrasenia"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["contrasenia"] = false
            }
            break
        default:
            break
        }
        
    }
    
    func FBButtonClicked(){
        let fbloginM : FBSDKLoginManager = FBSDKLoginManager()
        fbloginM.logInWithReadPermissions(["public_profile","email"], fromViewController: self) { (result, error) -> Void in
            let fbloginresult : FBSDKLoginManagerLoginResult = result
            
            if (error != nil){
                fbloginM.logOut()
                
            } else if  fbloginresult.isCancelled {
                
                fbloginM.logOut()
            } else {
                if result.grantedPermissions.contains("email")
                {
                    self.consultaUsuarioFB()
                }
            }
        }
    }
    
    func consultaUsuarioFB(){
        
        self.startActivityIndicator()
        
         FBSDKGraphRequest.init(graphPath: "me", parameters: nil).startWithCompletionHandler { (connection, result, error) -> Void in
         
            print (" datos de facebook \(result)")
            
            if (error != nil){
                let alertaError = UIAlertController(title: "Error", message: "A ocurrido un error al consultar los datos de la cuenta en FB", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: true, completion: nil)
                    let fbloginM : FBSDKLoginManager = FBSDKLoginManager()
                    fbloginM.logOut()
                }
                

            } else {
                
                let id_fb = (result.objectForKey("id") as? String)!
                 
                 var dtosJson = "{\"metodo\":\"consultarUsuario\","
                 dtosJson += "\"parametros\": [{"
                 dtosJson += "\"nombre\": \"facebook_id\","
                 dtosJson += "\"valor\": \"\(id_fb)\""
                 dtosJson += "}]"
                 dtosJson += "}"
                 
                 let xfunciones = funciones()
                 
                 let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
                 let session = NSURLSession.sharedSession()
                 
                 let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
                 let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
                 let base64LoginString = loginData.base64EncodedStringWithOptions([])
                 
                 let request = NSMutableURLRequest(URL: url)
                 request.HTTPMethod = Constantes.WSData.metodoHTTP
                 request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                 request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
                 request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
                 request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
                 
                 let task = session.dataTaskWithRequest(request) {
                 (let data, let response, let error) in
                 
                 guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                 print("A ocurrido un error: \(error)")
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                        let fbloginM : FBSDKLoginManager = FBSDKLoginManager()
                        fbloginM.logOut()
                    }
                 return
                 }
                 
                 do {
                 //deserializar los datos
                 let json = JSON(data: data!)
                 let resultado = json["resultado"].stringValue ?? ""
                 
                 if (resultado == "error"){
                    //no se encontro registro del usuario en la bd, intentar el registro con los datos de FB
                    self.registroUsuarioFB()
                }
                 else if (resultado == "ok"){
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                    }
                     //guardar datos de usuario
                     xfunciones.guardaDatosUsuario(json)
                     self.performSegueWithIdentifier("LoginToPrincipalSegue", sender: self)
                 
                 } else {
                    //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                    //enviar alerta de error
                 }
                 
                 
                 }
                }
                 
                 task.resume()
                
                
            }
         
         }
        
    }
    
    func registroUsuarioFB(){
        FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"first_name, id, email, picture.type(large)"]).startWithCompletionHandler { (connection, result, error) -> Void in
            
            if (error != nil){
                let alertaError = UIAlertController(title: "Error", message: "A ocurrido un error al consultar los datos de la cuenta en FB", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: true, completion: nil)
                    let fbloginM : FBSDKLoginManager = FBSDKLoginManager()
                    fbloginM.logOut()
                }
                
                
            } else {
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                }
                self.creandoCtaFB = true
                self.idFB = (result.objectForKey("id") as? String)!
                self.nombreFB = (result.objectForKey("first_name") as? String)!
                self.emailFB = (result.objectForKey("email") as? String)!
                self.fotoFB = (result.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as? String)!
                self.performSegueWithIdentifier("LoginToCrearCuentaSegue", sender: self)
                
            }
        }
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "LoginToCrearCuentaSegue" {
            if (self.creandoCtaFB){
                let crearCuentaVC = segue.destinationViewController as! crearCuenta2VC
                
                crearCuentaVC.ctaFB = true
                crearCuentaVC.nombreFB = self.nombreFB
                crearCuentaVC.emailFB = self.emailFB
                crearCuentaVC.idFB = self.idFB
                crearCuentaVC.fotoFB = self.fotoFB
            }
        }
    }
    
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }


} //fin de clase

extension UIImage {
    func scaleToSize(aSize :CGSize) -> UIImage {
        if (CGSizeEqualToSize(self.size, aSize)) {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0)
        self.drawInRect(CGRectMake(0.0, 0.0, aSize.width, aSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
