//
//  crearCuenta2VC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 23/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import AMPopTip
//import PureJsonSerializer
import SwiftyJSON
import Kingfisher


class crearCuenta2VC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var txtCedula: UITextField!
    @IBOutlet weak var txtCorreoe: UITextField!
    @IBOutlet weak var txtDireccionTrabajo: UITextField!
    @IBOutlet weak var txtFNacimiento: UITextField!
    @IBOutlet weak var txtLugarResidencia: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtCelular: UITextField!
    @IBOutlet weak var txtContrasenia: UITextField!
    @IBOutlet weak var txtVerificarContrasenia: UITextField!
    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var btnCrearCuenta: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var stackCampos: UIStackView!
    @IBOutlet weak var imgUsuario: UIImageView!
    
    //terminos
    @IBOutlet weak var containerTC: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblSaberMas: UILabel!
    var terminosChecked = false
    var backTransparente = UIView(frame: CGRectMake(0, 0, 0, 0))
    
    //image picker
    let imagePicker = UIImagePickerController()
    
    var datos_usuario : datosUsuario = datosUsuario()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    //array para los errores
    var boolsError: [String:Bool] = ["nombre":false,"apellidos":false,"cedula":false,"correo":false,"fechanac":false,"celular":false,"contrasenia":false,"confContra":false, "contraIgual":false ]
    var imgSeleccionada = false
    
    //para crear cuenta con FB
    var ctaFB = false
    var nombreFB = ""
    var emailFB = ""
    var idFB = ""
    var fotoFB = ""
    //var fechaNacFB = ""
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 109, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let paddingR = UIBarButtonItem(customView: UIView(frame: CGRectMake(0, 0, 50, 20)))
        
        self.navigationItem.rightBarButtonItem = paddingR
        
        //configuracion de controles
        self.txtNombre.delegate = self
        self.txtApellidos.delegate = self
        self.txtCedula.delegate = self
        self.txtCorreoe.delegate = self
        self.txtDireccionTrabajo.delegate = self
        self.txtFNacimiento.delegate = self
        self.txtLugarResidencia.delegate = self
        self.txtTelefono.delegate = self
        self.txtCelular.delegate = self
        self.txtContrasenia.delegate = self
        self.txtVerificarContrasenia.delegate = self
        
        //tags
        self.txtNombre.tag = 1
        self.txtApellidos.tag = 2
        self.txtCedula.tag = 3
        self.txtCorreoe.tag = 4
        self.txtDireccionTrabajo.tag = 5
        self.txtFNacimiento.tag = 6
        self.txtLugarResidencia.tag = 7
        self.txtTelefono.tag = 8
        self.txtCelular.tag = 9
        self.txtContrasenia.tag = 10
        self.txtVerificarContrasenia.tag = 11
        
        //date picker para la fecha de nacimiento
        let datePicker : UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.Date
        
        self.txtFNacimiento.inputView = datePicker
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), forControlEvents: .ValueChanged)
        
        //iconos para campo de password
        // icono derecho
        self.txtContrasenia.rightViewMode = UITextFieldViewMode.Always
        let txtPassHideShowBtn = UIButton(type: UIButtonType.Custom) as UIButton
        txtPassHideShowBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 10)
        let imgOjo = UIImage(named: "imgOjo")
        txtPassHideShowBtn.setImage(imgOjo, forState: .Normal)
        var insets : UIEdgeInsets = txtPassHideShowBtn.contentEdgeInsets
        insets.right += 5
        txtPassHideShowBtn.contentEdgeInsets = insets
        txtPassHideShowBtn.tag = 98
        txtPassHideShowBtn.addTarget(self, action: #selector(self.muestraOcultaPassword(_:)), forControlEvents: .TouchUpInside)
        self.txtContrasenia.rightView = txtPassHideShowBtn
        
        //icono izq
        self.txtContrasenia.leftViewMode = UITextFieldViewMode.Always
        let txtImgViewIP = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let imgCandado = UIImage(named: "imgCandado")
        txtImgViewIP.image = imgCandado
        self.txtContrasenia.leftView = txtImgViewIP
        
        //iconos para el campo confirmacion de password
        //icono derecho
        self.txtVerificarContrasenia.rightViewMode = UITextFieldViewMode.Always
        let txtConfPassHideShow = UIButton(type: UIButtonType.Custom) as UIButton
        txtConfPassHideShow.frame = CGRect(x: 0, y: 0, width: 20, height: 10)
        txtConfPassHideShow.setImage(imgOjo, forState: .Normal)
        insets = txtConfPassHideShow.contentEdgeInsets
        insets.right += 5
        txtConfPassHideShow.contentEdgeInsets = insets
        txtConfPassHideShow.tag = 99
        txtConfPassHideShow.addTarget(self, action: #selector(self.muestraOcultaPassword(_:)), forControlEvents: .TouchUpInside)
        //funcionalidad
        self.txtVerificarContrasenia.rightView = txtConfPassHideShow
        
        //icono izquierdo
        self.txtVerificarContrasenia.leftViewMode = UITextFieldViewMode.Always
        let txtImgViewIPC = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let imgCandadoVerif = UIImage(named: "imgCandadoVerficar")
        txtImgViewIPC.image = imgCandadoVerif
        txtImgViewIPC.contentMode = .ScaleAspectFit
        self.txtVerificarContrasenia.leftView = txtImgViewIPC
        
        //para image picker delegate
        self.imagePicker.delegate = self
        
        //accion de guardar al boton de crear cuenta
        self.btnCrearCuenta.addTarget(self, action: #selector(crearCuenta), forControlEvents: .TouchUpInside)
        
        //accion de traer la galeria para seleccionar imagen
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.seleccionaImagen))
        self.imgUsuario.userInteractionEnabled = true
        self.imgUsuario.addGestureRecognizer(tap)
        
        
        
        if (self.ctaFB){
            self.txtNombre.text = self.nombreFB
            self.txtNombre.userInteractionEnabled = false
            self.txtCorreoe.text = self.emailFB
            self.txtCorreoe.userInteractionEnabled = false
            let imgURL = NSURL(string: self.fotoFB)
            self.imgUsuario.kf_setImageWithURL(imgURL)
            self.imgUsuario.userInteractionEnabled = false
            self.txtContrasenia.hidden = true
            self.txtVerificarContrasenia.hidden = true
            self.imgSeleccionada = true
        }

        //para los onchange y limpiar validacion
        self.txtNombre.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtApellidos.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCedula.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCorreoe.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtFNacimiento.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCelular.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtContrasenia.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtVerificarContrasenia.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        let tapCheck = UITapGestureRecognizer(target: self, action: #selector(self.controlCheck))
        self.imgCheck.userInteractionEnabled = true
        self.imgCheck.addGestureRecognizer(tapCheck)
        let tapLblTC = UITapGestureRecognizer(target: self, action: #selector(self.controlCheck))
        self.lblSaberMas.userInteractionEnabled = true
        self.lblSaberMas.addGestureRecognizer(tapLblTC)
    }
    
    func controlCheck(){
        if (self.terminosChecked){
            self.imgCheck.image = UIImage(named: "checkBox_unchecked")
            self.terminosChecked = false
        } else {
            self.muestraTerminos()
            self.imgCheck.image = UIImage(named: "checkBox_checked")
            self.terminosChecked = true
            
        }
    }
    
    func muestraOcultaPassword(sender: UIButton){
        let tag = sender.tag
        switch tag {
        case 98:
            if (self.txtContrasenia.secureTextEntry){
                self.txtContrasenia.secureTextEntry = false
            } else {
                self.txtContrasenia.secureTextEntry = true
            }
            break
        case 99:
            if (self.txtVerificarContrasenia.secureTextEntry){
                self.txtVerificarContrasenia.secureTextEntry = false
            } else {
                self.txtVerificarContrasenia.secureTextEntry = true
            }
            break
        default: break
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        //self.txtNombre.addTarget(self, action: #selector(self.tapOnTextField(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        //self.txtApellidos.addTarget(self, action: #selector(self.tapOnTextField), forControlEvents: UIControlEvents.TouchDown)
        //observers para mover la pantalla cuando el teclado aparece o se oculta
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        //Ocultar si esta abierto el teclado
        self.hideKeyboard()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        
        if (self.isMovingFromParentViewController()){
            if (self.ctaFB){
                let fbloginM : FBSDKLoginManager = FBSDKLoginManager()
                fbloginM.logOut()
            }
        }
    }
    
    //MARK: Para ocultar teclado
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Dismisses the Keyboard by making the text field resign
        // first responder
        textField.resignFirstResponder()
        
        // returns false. Instead of adding a line break, the text
        // field resigns
        return false
    }
    
    /*
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.scrollContainer.endEditing(true)
        self.view.endEditing(true)
    }
 */
    //para ocultar teclado
    
    //para mover la pantalla cuando se muestra el teclado
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollContainer.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollContainer.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        var contentInset:UIEdgeInsets = UIEdgeInsetsZero
        contentInset.top += 64 //se respeta el offset que trae el constraint de container view respecto al top
        self.scrollContainer.contentInset = contentInset
    }
    //para mover la pantalla cuando se muestra el teclado
    
    //MARK: Para el datepicker select
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.dateFormat = "yyyy-M-d"
        
        self.txtFNacimiento.text = dateFormatter.stringFromDate(sender.date)
        
        /*
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.fechaNacFB = dateFormatter.stringFromDate(sender.date)
        */
    }
    
    //para el date picker select
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    //MARK: para generar json
    func generaJsonDatosUsuario()->String{
        
        let xfunciones = funciones()
        
        let paraJson: [String: AnyObject] = [
            "metodo": "insertarUsuario",
            "parametros": [
                [
                "nombre": Constantes.JsonData.usuario.email,
                "valor": self.datos_usuario.correo
                ],
                [
                "nombre":Constantes.JsonData.usuario.password,
                "valor": self.datos_usuario.contrasenia
                ],
                [
                    "nombre":Constantes.JsonData.usuario.token,
                    "valor": " "
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.nombre,
                    "valor": self.datos_usuario.nombre
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.apellido,
                    "valor": self.datos_usuario.apellidos
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.fecha_nacimiento,
                    "valor": self.datos_usuario.fecha_nac
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.direccion_casa,
                    "valor": self.datos_usuario.lugar_residencia
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.direccion_trabajo,
                    "valor": self.datos_usuario.direccion_trabajo
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.telefono,
                    "valor": self.datos_usuario.telefono
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.celular,
                    "valor": self.datos_usuario.celular
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.correo_personal,
                    "valor": self.datos_usuario.correo
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.cedula,
                    "valor": self.datos_usuario.cedula
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.foto,
                    "valor": xfunciones.imgToBase64(self.datos_usuario.foto)
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            return jsonString
        } else {
            return ""
        }
        
    }
    
    func generaJsonDatosUsuarioFB()->String{
        
        let paraJson: [String: AnyObject] = [
            "metodo": "insertarUsuario",
            "parametros": [
                [
                    "nombre": Constantes.JsonData.datosUsuario.correo_personal,
                    "valor": self.emailFB
                ],
                [
                    "nombre":Constantes.JsonData.usuario.facebook_id,
                    "valor": self.idFB
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.nombre,
                    "valor": self.datos_usuario.nombre
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.apellido,
                    "valor": self.datos_usuario.apellidos
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.fecha_nacimiento,
                    "valor": self.datos_usuario.fecha_nac
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.direccion_casa,
                    "valor": self.datos_usuario.lugar_residencia
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.direccion_trabajo,
                    "valor": self.datos_usuario.direccion_trabajo
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.telefono,
                    "valor": self.datos_usuario.telefono
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.celular,
                    "valor": self.datos_usuario.celular
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.cedula,
                    "valor": self.datos_usuario.cedula
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.foto,
                    "valor": self.fotoFB
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            return jsonString
        } else {
            return ""
        }
        
    }
    
    //MARK: Accion de crear cuenta, envio de datos
    func crearCuenta(){
        if (self.esValidaLaForma()){
            
            
            
            if (!self.terminosChecked){
                
                let alertaError = UIAlertController(title: "", message: "Lea los términos y condiciones", preferredStyle: .ActionSheet)
                self.presentViewController(alertaError, animated: true, completion: nil)
                
                let delay = 2 * Double(NSEC_PER_SEC)
                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                dispatch_after(time, dispatch_get_main_queue()) { () -> Void in
                    alertaError.dismissViewControllerAnimated(true, completion: nil)
                }
                return
            }
            
            self.startActivityIndicator()
            
            var dtosJson = ""
            if (ctaFB){
                dtosJson = self.generaJsonDatosUsuarioFB()
            } else {
                dtosJson = self.generaJsonDatosUsuario()
            }
            
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                return
            }
            //preparar los datos para envio
            
            let xfunciones = funciones()
            
            //para el envio de datos
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
            
            //envio de datos
            
            let task = session.dataTaskWithRequest(request) {
                ( data, response, error) in
                
                let httpResponse = response as? NSHTTPURLResponse
                
                if httpResponse?.statusCode != 200 {
                    print(httpResponse?.statusCode)
                }
                
                if ( error != nil ) {
                    print("Localized description error: \(error!.localizedDescription)")
                    //quitar datos al user defaults por si tuviera
                    xfunciones.BorraDatosUsuario()
                    let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar realizar el registro", preferredStyle: .Alert)
                    let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                        //No hacer nada por ahora
                    }
                    alertaError.addAction(okActionError)
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                        self.presentViewController(alertaError, animated: true, completion: nil)
                    }
                    
                } else {
                    do {
                        //deserializar los datos
                        let json = JSON(data: data!)
                        let resultado = json["resultado"].stringValue ?? ""
                        
                        //quitar el loader
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                        }
                        
                        
                        if (resultado == "error"){
                            //quitar datos al user defaults por si tuviera
                            xfunciones.BorraDatosUsuario()
                            
                            var texto = json["texto"].stringValue ?? "Ocurrio un error al intentar realizar el registro"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            dispatch_async(dispatch_get_main_queue()){
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }
                            
                        }
                            
                        else if (resultado == "ok"){
                            //guardar datos de usuario
                            xfunciones.guardaDatosUsuario(json)
                            self.performSegueWithIdentifier("crearCuentaToPrincipalSegue", sender: self)
                            
                        } else {
                            //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                            //enviar alerta de error
                            print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                        }
                        
                    }                     
                }
            } //hasta aqui el data task request
            
            task.resume()
            
            //hasta aca envio de datos
            

            
        } //if es valida la forma
    }
    
    func limpiaArrayError(){
        for dicCode in self.boolsError.keys{
            self.boolsError[dicCode] = false
        }
    }
    
    func esValidaLaForma()->Bool{
        //nombre, apellidos, cedula, correo, fechanac, celular, cont	rasenia y confContra, imagen, contraIgual
        var error = false
        limpiaArrayError()
        
        if (!self.imgSeleccionada){
            error = true
            let tte_img = AMPopTip()
            tte_img.shouldDismissOnTap = true
            tte_img.shouldDismissOnTapOutside = true
            tte_img.arrowSize = CGSizeMake(0, 0)
            tte_img.showText("Imagen requerida", direction: .Down, maxWidth: 200, inView: self.viewContainer, fromFrame: self.btnCrearCuenta.frame, duration: 3)
        }
        let xfunciones = funciones()
        let nombre = self.txtNombre.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (nombre == "" || nombre?.characters.count < 4){
            xfunciones.markInvalid(self.txtNombre)
            xfunciones.shakeView(self.txtNombre)
            if (!error) { error = true }
            self.boolsError["nombre"] = true
        }
        
        let apellidos = self.txtApellidos.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (apellidos == "" || apellidos?.characters.count < 4){
            xfunciones.markInvalid(self.txtApellidos)
            xfunciones.shakeView(self.txtApellidos)
            self.boolsError["apellidos"] = true
            if (!error) { error = true }
        }
        
        //pendiente tener el algoritmo de validacion
        let cedula = self.txtCedula.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if (cedula == ""){
            xfunciones.markInvalid(self.txtCedula)
            xfunciones.shakeView(self.txtCedula)
            self.boolsError["cedula"] = true
            if (!error) { error = true }
        }
        
        let correo = self.txtCorreoe.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (!xfunciones.isValidEmailAddress(correo!) || correo?.characters.count < 4){
            xfunciones.markInvalid(self.txtCorreoe)
            xfunciones.shakeView(self.txtCorreoe)
            self.boolsError["correo"] = true
            if (!error) { error = true }
        }
        
        let fecha_nac = self.txtFNacimiento.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (fecha_nac == "" || fecha_nac?.characters.count < 4){
            xfunciones.markInvalid(self.txtFNacimiento)
            xfunciones.shakeView(self.txtFNacimiento)
            self.boolsError["fechanac"] = true
            if (!error) { error = true }
        }
        
        let numcelular = self.txtCelular.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (numcelular == "" || numcelular?.characters.count < 4){
            xfunciones.markInvalid(self.txtCelular)
            xfunciones.shakeView(self.txtCelular)
            self.boolsError["celular"] = true
            if (!error) { error = true }
        }
        
        var contrasenia = ""
        
        if (!self.ctaFB) {
            contrasenia = (self.txtContrasenia.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()))!
            
            if (contrasenia.characters.count < 4){
                xfunciones.markInvalid(self.txtContrasenia)
                xfunciones.shakeView(self.txtContrasenia)
                self.boolsError["contrasenia"] = true
                if (!error) { error = true }
                
            }
            
            let confContrasenia = self.txtVerificarContrasenia.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            if (confContrasenia?.characters.count < 4){
                xfunciones.markInvalid(self.txtVerificarContrasenia)
                xfunciones.shakeView(self.txtVerificarContrasenia)
                self.boolsError["confContra"] = true
                if (!error) { error = true }
                
            }
            
            if (contrasenia != confContrasenia!){
                xfunciones.markInvalid(self.txtContrasenia)
                xfunciones.markInvalid(self.txtVerificarContrasenia)
                xfunciones.shakeView(self.txtContrasenia)
                xfunciones.shakeView(self.txtVerificarContrasenia)
                self.boolsError["contraIgual"] = true
                if (!error) { error = true }
            }
        }
        
        if !error {
            //datos validados
            self.datos_usuario = datosUsuario()
            self.datos_usuario.nombre = nombre!
            self.datos_usuario.apellidos = apellidos!
            self.datos_usuario.cedula = cedula!
            self.datos_usuario.correo = correo!
            self.datos_usuario.fecha_nac = fecha_nac!
            self.datos_usuario.celular = numcelular!
            if (!self.ctaFB){
                self.datos_usuario.contrasenia = contrasenia
            } else {
                self.datos_usuario.contrasenia = ""
            }
            self.datos_usuario.foto = self.imgUsuario.image!
            
            //datos no validados
            if let dirTrabajo = self.txtDireccionTrabajo.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()){
                self.datos_usuario.direccion_trabajo = dirTrabajo
            }
            
            if let lugarResidencia = self.txtLugarResidencia.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()){
                self.datos_usuario.lugar_residencia = lugarResidencia
            }
            
            if let telefono = self.txtTelefono.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()){
                self.datos_usuario.telefono = telefono
            }
        }
        
        
        return !error
    }
    
    func textFieldShouldBeginEditing(xtextField: UITextField) -> Bool {
        
        let tiempo : NSTimeInterval = 2
        
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["nombre"]) != nil) && (self.boolsError["nombre"] == true){
                let tte_nombre = AMPopTip()
                tte_nombre.shouldDismissOnTap = true
                tte_nombre.shouldDismissOnTapOutside = true
                tte_nombre.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtNombre.frame, duration: tiempo)
            }
            break
        case 2:
            if ((self.boolsError["apellidos"]) != nil) && (self.boolsError["apellidos"] == true){
                let tte_app = AMPopTip()
                tte_app.shouldDismissOnTap = true
                tte_app.shouldDismissOnTapOutside = true
                tte_app.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtApellidos.frame, duration: tiempo)
            }
            break
        case 3:
            if ((self.boolsError["cedula"]) != nil) && (self.boolsError["cedula"] == true){
                let tte_ced = AMPopTip()
                tte_ced.shouldDismissOnTap = true
                tte_ced.shouldDismissOnTapOutside = true
                tte_ced.showText("No puede ingresar caracteres en el número", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCedula.frame, duration: tiempo)
            }
            break
        case 4:
            if ((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true){
                let tte_ce = AMPopTip()
                tte_ce.shouldDismissOnTap = true
                tte_ce.shouldDismissOnTapOutside = true
                tte_ce.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCorreoe.frame, duration: tiempo)
            }
            break
        case 6:
            if ((self.boolsError["fechanac"]) != nil) && (self.boolsError["fechanac"] == true){
                let tte_fn = AMPopTip()
                tte_fn.shouldDismissOnTap = true
                tte_fn.shouldDismissOnTapOutside = true
                tte_fn.showText("Este campo es requerido", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtFNacimiento.frame, duration: tiempo)
            }
            break
        case 9:
            if ((self.boolsError["celular"]) != nil) && (self.boolsError["celular"] == true){
                let tte_cel = AMPopTip()
                tte_cel.shouldDismissOnTap = true
                tte_cel.shouldDismissOnTapOutside = true
                tte_cel.showText("Ingrese información", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCelular.frame, duration: tiempo)
            }
            break
        case 10:
            if ((self.boolsError["contrasenia"]) != nil) && (self.boolsError["contrasenia"] == true){
                let tte_contra = AMPopTip()
                tte_contra.shouldDismissOnTap = true
                tte_contra.shouldDismissOnTapOutside = true
                tte_contra.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtContrasenia.frame, duration: tiempo)
            } else if ((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true){
                let tte_contra = AMPopTip()
                tte_contra.shouldDismissOnTap = true
                tte_contra.shouldDismissOnTapOutside = true
                tte_contra.showText("Contraseñas diferentes", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtContrasenia.frame, duration: tiempo)
            }
            break
        case 11:
            if ((self.boolsError["confContra"]) != nil) && (self.boolsError["confContra"] == true){
                let tte_confcontra = AMPopTip()
                tte_confcontra.shouldDismissOnTap = true
                tte_confcontra.shouldDismissOnTapOutside = true
                tte_confcontra.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtVerificarContrasenia.frame, duration: tiempo)
            } else if ((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true){
                let tte_confcontra = AMPopTip()
                tte_confcontra.shouldDismissOnTap = true
                tte_confcontra.shouldDismissOnTapOutside = true
                tte_confcontra.showText("Contraseñas diferentes", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtVerificarContrasenia.frame, duration: tiempo)
            }
            break

        default:
            break
        }
        return true
    }
    
    //MARK: para los on change de los textfields
    func textFieldDidChange(xtextField: UITextField) {
        let xfunciones = funciones()
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["nombre"]) != nil) && (self.boolsError["nombre"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["nombre"] = false
            }
            break
        case 2:
            if ((self.boolsError["apellidos"]) != nil) && (self.boolsError["apellidos"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["apellidos"] = false
            }
            break
        case 3:
            if ((self.boolsError["cedula"]) != nil) && (self.boolsError["cedula"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["cedula"] = false
            }
            break
            
        case 4:
            if ( (((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["correo"] = false
            }
            break
        case 6:
            if ( (((self.boolsError["fechanac"]) != nil) && (self.boolsError["fechanac"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["fechanac"] = false
            }
            break
        case 9:
            if ( (((self.boolsError["celular"]) != nil) && (self.boolsError["celular"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["celular"] = false
            }
            break
        case 10:
            if ( (((self.boolsError["contrasenia"]) != nil) && (self.boolsError["contrasenia"] == true)) || (((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["contrasenia"] = false
                self.boolsError["contraIgual"] = false
            }
            break
        case 11:
            if ( (((self.boolsError["confContra"]) != nil) && (self.boolsError["confContra"] == true)) || (((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["confContra"] = false
                self.boolsError["contraIgual"] = false
            }
            break

            
        default:
            break
        }
        
    }


    //MARK: Para image picker
    func seleccionaImagen(sender: UITapGestureRecognizer){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .PhotoLibrary
            presentViewController(self.imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let imagenSelec = info[UIImagePickerControllerOriginalImage] as? UIImage {
            dispatch_async(dispatch_get_main_queue(), {
                let nuevaImg = self.scaleAndCropImage(imagenSelec, toSize: CGSizeMake(300, 300))
                self.datos_usuario.foto = nuevaImg
                self.imgUsuario.image = nuevaImg
                self.imgUsuario.setNeedsDisplay()
            })
            self.imgSeleccionada = true
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // finally dismiss the Imagepicker controller
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //para image picker
    
    //PAra resize de la imagen
    func scaleAndCropImage(image:UIImage, toSize size: CGSize) -> UIImage {
        // Sanity check; make sure the image isn't already sized.
        if CGSizeEqualToSize(image.size, size) {
            return image
        }
        
        let widthFactor = size.width / image.size.width
        let heightFactor = size.height / image.size.height
        var scaleFactor: CGFloat = 0.0
        
        scaleFactor = heightFactor
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }
        
        var thumbnailOrigin = CGPointZero
        let scaledWidth  = image.size.width * scaleFactor
        let scaledHeight = image.size.height * scaleFactor
        
        if widthFactor > heightFactor {
            thumbnailOrigin.y = (size.height - scaledHeight) / 2.0
        }
            
        else if widthFactor < heightFactor {
            thumbnailOrigin.x = (size.width - scaledWidth) / 2.0
        }
        
        var thumbnailRect = CGRectZero
        thumbnailRect.origin = thumbnailOrigin
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.drawInRect(thumbnailRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func muestraTerminos(){
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        backTransparente.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        backTransparente.center = self.view.center
        // Change background color and alpha channel here
        backTransparente.backgroundColor = UIColor.blackColor()
        backTransparente.clipsToBounds = true
        backTransparente.alpha = 0.5
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.ocultaTerminos))
        backTransparente.addGestureRecognizer(tap)
        view.addSubview(backTransparente)
        
        self.containerTC.hidden = false
        self.view.bringSubviewToFront(self.containerTC)
    }
    
    func ocultaTerminos(){
        backTransparente.removeFromSuperview()
        self.containerTC.hidden = true
    }


}// fin de clase
