//
//  celdaPremiosDispTVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 30/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class celdaPremiosDispTVC: UITableViewCell {
    @IBOutlet weak var imgPremio: UIImageView!
    @IBOutlet weak var lblPremio: UILabel!
    @IBOutlet weak var lblCantMonedas: UILabel!
    @IBOutlet weak var imgMoneda: UIImageView!
    @IBOutlet weak var lblNivel: UILabel!
    @IBOutlet weak var btnCanjear: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
