//
//  PremiosDispVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 30/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import Kingfisher
import PureJsonSerializer

class PremiosDispVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableViewPremiosDisp: UITableView!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    //arreglo de premios
    var itemsPremiosDisp = [premioDisponible]()
    var btnCanjear = [UIButton]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewPremiosDisp.dataSource = self
        self.tableViewPremiosDisp.delegate = self
        
        //ocultar celdas vacias
        self.tableViewPremiosDisp.tableFooterView = UIView(frame: CGRectZero)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.startActivityIndicator()
        self.cargaDatos()
    }
    
    //MARK: Carga datos del WS
    func cargaDatos(){
        var dtosJson = "{\"metodo\":\"premiosDisponiblesXUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                }
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los premios disponibles", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                    }
                    if (resultado == "error"){
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar obtener los premios disponibles"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.itemsPremiosDisp.removeAll()
                        self.btnCanjear.removeAll()
                        if (!(json["lista"]?.isNull)!){
                            for item in (json["lista"]?.arrayValue)! {
                                let unPremio = premioDisponible()
                                unPremio.premio_id = (item["premio_id"]?.stringValue)!
                                unPremio.nombre = (item["nombre"]?.stringValue)!.uppercaseString
                                unPremio.cantidad = (item["cantidad"]?.stringValue)! //cantidad de monedas
                                unPremio.punto_id = (item["punto_id"]?.stringValue)!
                                if (item["fecha_inicio"]?.stringValue)! != nil {
                                    unPremio.fecha_inicio = (item["fecha_inicio"]?.stringValue)!
                                } else {
                                    unPremio.fecha_inicio = ""
                                }
                                
                                if (item["fecha_fin"]?.stringValue)! != nil {
                                    unPremio.fecha_fin = (item["fecha_fin"]?.stringValue)!
                                } else {
                                    unPremio.fecha_fin = ""
                                }
                                
                                unPremio.punto_id = (item["fecha_creacion"]?.stringValue)!
                                unPremio.stock = (item["stock"]?.stringValue)!
                                unPremio.nivel_id = (item["nivel_id"]?.stringValue)!
                                unPremio.url_premio = (item["url_premio"]?.stringValue)! //imagen del premio
                                
                                if (item["nota"]?.stringValue)! != nil {
                                    unPremio.nota = (item["nota"]?.stringValue)!
                                } else {
                                    unPremio.nota = ""
                                }
                                
                                unPremio.estado_id = (item["estado_id"]?.stringValue)!
                                unPremio.punto = (item["punto"]?.stringValue)!
                                unPremio.url_imagen = (item["url_imagen"]?.stringValue)! //imagen de la moneda
                                unPremio.usuario_id = (item["usuario_id"]?.stringValue)!
                                self.itemsPremiosDisp.append(unPremio)
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.stopActivityIndicator()
                                self.tableViewPremiosDisp.reloadData()
                            })
                        } else {
                            NSLog("se consulto los premios disp correctamente, pero la lista llego null")
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.itemsPremiosDisp.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("celdaPremiosDisp", forIndexPath: indexPath) as! celdaPremiosDispTVC
        
        let itemPremio = self.itemsPremiosDisp[indexPath.row]
        
        var imgURL = NSURL(string: itemPremio.url_premio)
        cell.imgPremio.kf_setImageWithURL(imgURL)
        
        imgURL = NSURL(string: itemPremio.url_imagen)
        cell.imgMoneda.kf_setImageWithURL(imgURL)
        
        cell.lblCantMonedas.text = itemPremio.cantidad
        cell.lblPremio.text = itemPremio.nombre
        cell.lblNivel.text = "Nivel: \(itemPremio.nivel_id)"
        
        cell.btnCanjear.addTarget(self, action: #selector(self.canjearPremio(_:)), forControlEvents: .TouchUpInside)
        self.btnCanjear.append(cell.btnCanjear)
        
        //espacio a la celda
        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height-10))
        whiteRoundedView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 0.7])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
        //espacio a las celdas
        
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        _ = indexPath.row
        
    }
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    //MARK: Canjear premio
    func canjearPremio(sender:UIButton){
        let elIndice = self.btnCanjear.indexOf(sender)
        if elIndice != nil {
            let idPremio = self.itemsPremiosDisp[elIndice!].premio_id
            let dtosJson = generaJson(idPremio)
            
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                return
            }

            self.startActivityIndicator()
            
            //para el envio de datos
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
            
            //envio de datos
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                let httpResponse = response as? NSHTTPURLResponse
                
                if httpResponse?.statusCode != 200 {
                    print(httpResponse?.statusCode)
                }
                
                if ( error != nil ) {
                    print("Localized description error: \(error!.localizedDescription)")
                    
                    let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar canjear el premio", preferredStyle: .Alert)
                    let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                        //No hacer nada por ahora
                    }
                    alertaError.addAction(okActionError)
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                        self.presentViewController(alertaError, animated: false, completion: nil)
                    }
                    
                } else {
                    do {
                        //deserializar los datos
                        let json = try Json.deserialize(data!)
                        let resultado = json["resultado"]?.stringValue ?? ""
                        
                        if (resultado == "error"){
                            
                            
                            var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar canjear el premio"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            
                            //poner alter error
                            dispatch_async(dispatch_get_main_queue()){
                                self.stopActivityIndicator()
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }
                        }
                            
                        else if (resultado == "ok"){
                            
                            
                            var texto = json["texto"]?.stringValue ?? "Se ha reclamado el premio con éxito"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaOK = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let alertaOKAccion = UIAlertAction(title: "OK", style: .Cancel) { action in
                                self.startActivityIndicator()
                                self.cargaDatos()
                            }
                            alertaOK.addAction(alertaOKAccion)
                            
                            //poner alter ok
                            dispatch_async(dispatch_get_main_queue()){
                                self.stopActivityIndicator()
                                self.presentViewController(alertaOK, animated: true, completion: nil)
                                
                            }
                            
                            
                        } else {
                            //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                            //enviar alerta de error
                            print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                        }
                        
                    } catch {
                        print("Json serialization failed with error: \(error)")
                    }
                    
                }
            } //hasta aqui el data task request
            
            task.resume()
            
            //hasta aca envio de datos
        }
    }
    
    //MARK: para generar el json
    func generaJson(id_premio:String)->String{
        let paraJson: [String: AnyObject] = [
            "metodo": "redimirPremio",
            "parametros": [
                [
                    "nombre":Constantes.JsonData.usuario.usuario_id,
                    "valor": self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String
                ],
                [
                    "nombre":"premio_id",
                    "valor": id_premio
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            
            return jsonString
        } else {
            return ""
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

} //fin de clase

class premioDisponible{
    var premio_id = ""
    var nombre = ""
    var cantidad = ""
    var punto_id = ""
    var fecha_inicio = ""
    var fecha_fin = ""
    var fecha_creacion = ""
    var stock = ""
    var nivel_id = ""
    var url_premio = ""
    var nota = ""
    var estado_id = ""
    var punto = ""
    var url_imagen = ""
    var usuario_id = ""
}

/*public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
}

}
*/
