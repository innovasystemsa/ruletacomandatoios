//
//  PremiosContainerVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 31/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class PremiosContainerVC: UIViewController {
    @IBOutlet weak var btnPremiosDisp: UIButton!
    @IBOutlet weak var btnPremiosObtenidos: UIButton!
    @IBOutlet weak var btnPremiosPromo: UIButton!
    
    var marcadorPremiosDisp = UIView()
    var marcadorPremiosObtenidos = UIView()
    var marcadorPremiosPromo = UIView()
    
    let verdeMarcador = UIColor(red: 107/255, green: 173/255, blue: 44/255, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
        self.btnPremiosDisp.titleLabel?.textAlignment = .Center
        self.btnPremiosDisp.tag = 0
        self.btnPremiosDisp.addTarget(self, action: #selector(self.clickBtnCabecera(_:)), forControlEvents: .TouchUpInside)
        
        self.btnPremiosObtenidos.titleLabel?.textAlignment = .Center
        self.btnPremiosObtenidos.tag = 1
        self.btnPremiosObtenidos.addTarget(self, action: #selector(self.clickBtnCabecera(_:)), forControlEvents: .TouchUpInside)
        
        self.btnPremiosPromo.titleLabel?.textAlignment = .Center
        self.btnPremiosPromo.tag = 2
        self.btnPremiosPromo.addTarget(self, action: #selector(self.clickBtnCabecera(_:)), forControlEvents: .TouchUpInside)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.changeMarker), name: "cambiarMarcador", object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "cambiarMarcador", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.marcadorPremiosDisp = UIView(frame: CGRectMake(0,self.btnPremiosDisp.frame.size.height - 2 , self.btnPremiosDisp.frame.size.width ,2))
        self.marcadorPremiosObtenidos = UIView(frame: CGRectMake(0,self.btnPremiosObtenidos.frame.size.height - 2 , self.btnPremiosObtenidos.frame.size.width ,2))
        self.marcadorPremiosPromo = UIView(frame: CGRectMake(0,self.btnPremiosPromo.frame.size.height - 2 , self.btnPremiosPromo.frame.size.width ,2))
        
        self.btnPremiosDisp.addSubview(self.marcadorPremiosDisp)
        self.btnPremiosObtenidos.addSubview(self.marcadorPremiosObtenidos)
        self.btnPremiosPromo.addSubview(self.marcadorPremiosPromo)
        
        clearMarcador()

    }
    
    func clickBtnCabecera(sender: UIButton){
        let eltag = sender.tag
        clearMarcador()
        ponerMarcador(String(eltag))
        NSNotificationCenter.defaultCenter().postNotificationName("cambiaPagina", object: nil, userInfo: ["pagina":"\(eltag)"])
    }
    
    func changeMarker(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String, String!>
        clearMarcador()
        if let pagina = userInfo["pagina"] {
            
            ponerMarcador(pagina)
        }
    }
    
    func clearMarcador(){
        self.marcadorPremiosDisp.backgroundColor = UIColor.clearColor()
        self.marcadorPremiosObtenidos.backgroundColor = UIColor.clearColor()
        self.marcadorPremiosPromo.backgroundColor = UIColor.clearColor()
    }
    
    func ponerMarcador(pagina: String){
        switch pagina {
        case "0": self.marcadorPremiosDisp.backgroundColor = verdeMarcador
        case "1": self.marcadorPremiosObtenidos.backgroundColor = verdeMarcador
        case "2": self.marcadorPremiosPromo.backgroundColor = verdeMarcador
        default: clearMarcador()
        }
    }
    
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
