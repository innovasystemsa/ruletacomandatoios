//
//  OlvideContraViewController.swift
//  Comandato
//
//  Created by Leonel Sánchez on 13/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import AMPopTip
import PureJsonSerializer

class OlvideContraViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var navigationBarItem: UINavigationItem!
    @IBOutlet weak var scrollViewContainer: UIScrollView!
    @IBOutlet weak var ViewContainer: UIView!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtCedula: UITextField!
    @IBOutlet weak var txtNuevaContra: UITextField!
    @IBOutlet weak var txtRepetirContra: UITextField!
    @IBOutlet weak var stackCampos: UIStackView!
    @IBOutlet weak var btnCambiarContra: UIButton!
    
    //array para los errores
    var boolsError: [String:Bool] = ["cedula":false,"correo":false,"contrasenia":false,"confContra":false, "contraIgual":false ]
    var cedula_usr = ""
    var correo_usr = ""
    var contrasenia_usr = ""
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        navigationBarItem.titleView = bannerImgView
        
        //asignar delegates
        self.txtCedula.delegate = self
        self.txtCorreo.delegate = self
        self.txtNuevaContra.delegate = self
        self.txtRepetirContra.delegate = self
        
        //asignar tags
        self.txtCorreo.tag = 1
        self.txtCedula.tag = 2
        self.txtNuevaContra.tag = 3
        self.txtRepetirContra.tag = 4
        
        //accion cambiar contraseña al boton
        self.btnCambiarContra.addTarget(self, action: #selector(self.cambiarContrasenia), forControlEvents: .TouchUpInside)
        
        //asignar los onchange a los textfield
        self.txtCorreo.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtCedula.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtNuevaContra.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        self.txtRepetirContra.addTarget(self, action: #selector(self.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.btnCambiarContra.layer.cornerRadius = 5
        self.btnCambiarContra.layer.borderWidth = 1
        self.btnCambiarContra.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    override func viewDidAppear(animated: Bool) {
        //mover el teclado
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        self.hideKeyboard()

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
 
    
    //MARK: Para ocultar teclado
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Dismisses the Keyboard by making the text field resign
        // first responder
        textField.resignFirstResponder()
        
        // returns false. Instead of adding a line break, the text
        // field resigns
        return false
    }
    
    /*
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.ViewContainer.endEditing(true)
        self.view.endEditing(true)
    }
 */
 
    //para ocultar teclado
    
    
    //para mover la pantalla cuando se muestra el teclado
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollViewContainer.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollViewContainer.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollViewContainer.contentInset = contentInset
    }
    //para mover la pantalla cuando se muestra el teclado
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func limpiaArrayError(){
        for dicCode in self.boolsError.keys{
            self.boolsError[dicCode] = false
        }
    }
    
    //MARK: Accion de cambiar la contraseña, envio de datos
    func cambiarContrasenia(){
        if (self.esValidaLaForma()){
            
            let dtosJson = self.generaJsonDatosUsuario()
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                return
            }
            //preparar los datos para envio
            
            let xfunciones = funciones()
            
            //loader de carga
            let alert = UIAlertController(title: nil, message: "Por favor espere...", preferredStyle: .Alert)
            
            alert.view.tintColor = UIColor.blackColor()
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(10, 5, 50, 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            presentViewController(alert, animated: true, completion: nil)
            
            //para el envio de datos
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
            
            //envio de datos
            
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                let httpResponse = response as? NSHTTPURLResponse
                
                if httpResponse?.statusCode != 200 {
                    print(httpResponse?.statusCode)
                }
                
                if ( error != nil ) {
                    print("Localized description error: \(error!.localizedDescription)")
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        alert.dismissViewControllerAnimated(true, completion: nil)
                    }
                    //quitar datos al user defaults por si tuviera
                    xfunciones.BorraDatosUsuario()
                    let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar realizar el cambio de contraseña", preferredStyle: .Alert)
                    let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                        //No hacer nada por ahora
                    }
                    alertaError.addAction(okActionError)
                    self.presentViewController(alertaError, animated: true, completion: nil)
                    
                    
                } else {
                    do {
                        //deserializar los datos
                        let json = try Json.deserialize(data!)
                        let resultado = json["resultado"]?.stringValue ?? ""
                        
                        
                        if (resultado == "error"){
                            
                            //quitar datos al user defaults por si tuviera
                            xfunciones.BorraDatosUsuario()
                            
                            var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar realizar el cambio de contraseña"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            //quitar el loader
                            dispatch_async(dispatch_get_main_queue()){
                                alert.dismissViewControllerAnimated(true, completion: nil)
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }
                            
                        }
                            
                        else if (resultado == "ok"){
                            
                            var texto = json["texto"]?.stringValue ?? "Se ha cambiado correctamente la contraseña"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            
                            dispatch_async(dispatch_get_main_queue()){
                                alert.dismissViewControllerAnimated(true, completion: nil)
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }   
                            
                        } else {
                            //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                            //enviar alerta de error
                            print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                        }
                        
                    } catch {
                        print("Json serialization failed with error: \(error)")
                    }
                    
                }
            } //hasta aqui el data task request
            
            task.resume()
            
            //hasta aca envio de datos

            

        }
    }
    
    //MARK: para generar el json
    func generaJsonDatosUsuario()->String{
        let paraJson: [String: AnyObject] = [
            "metodo": "olvideContrasenia",
            "parametros": [
                [
                    "nombre":Constantes.JsonData.usuario.password,
                    "valor": self.contrasenia_usr
                ],
                [
                    "nombre":Constantes.JsonData.usuario.email,
                    "valor": self.correo_usr
                ],
                [
                    "nombre":Constantes.JsonData.datosUsuario.cedula,
                    "valor": self.cedula_usr
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            
            return jsonString
        } else {
            return ""
        }
        
    }
    
    
    //validar la forma
    func esValidaLaForma()->Bool{
        //cedula, correo, contra y confcontra
        var error = false
        limpiaArrayError()
        
        let xfunciones = funciones()
        //pendiente tener el algoritmo de validacion
        let cedula = self.txtCedula.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if (cedula == ""){
            xfunciones.markInvalid(self.txtCedula)
            xfunciones.shakeView(self.txtCedula)
            self.boolsError["cedula"] = true
            if (!error) { error = true }
        }
        
        let correo = self.txtCorreo.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (!xfunciones.isValidEmailAddress(correo!) || correo?.characters.count < 4){
            xfunciones.markInvalid(self.txtCorreo)
            xfunciones.shakeView(self.txtCorreo)
            self.boolsError["correo"] = true
            if (!error) { error = true }
        }
        
        let contrasenia = self.txtNuevaContra.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (contrasenia?.characters.count < 4){
            xfunciones.markInvalid(self.txtNuevaContra)
            xfunciones.shakeView(self.txtNuevaContra)
            self.boolsError["contrasenia"] = true
            if (!error) { error = true }
            
        }
        
        let confContrasenia = self.txtRepetirContra.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if (confContrasenia?.characters.count < 4){
            xfunciones.markInvalid(self.txtRepetirContra)
            xfunciones.shakeView(self.txtRepetirContra)
            self.boolsError["confContra"] = true
            if (!error) { error = true }
            
        }
        
        if (contrasenia! != confContrasenia!){
            xfunciones.markInvalid(self.txtNuevaContra)
            xfunciones.markInvalid(self.txtRepetirContra)
            xfunciones.shakeView(self.txtNuevaContra)
            xfunciones.shakeView(self.txtRepetirContra)
            self.boolsError["contraIgual"] = true
            if (!error) { error = true }
        }

        
        if (!error){
            //datos validados sin error
            self.cedula_usr = cedula!
            self.correo_usr = correo!
            self.contrasenia_usr = contrasenia!
        }

        
        
        return !error
    }
    
    func textFieldShouldBeginEditing(xtextField: UITextField) -> Bool {
        let tiempo : NSTimeInterval = 2
        
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true){
                let tte_ce = AMPopTip()
                tte_ce.shouldDismissOnTap = true
                tte_ce.shouldDismissOnTapOutside = true
                tte_ce.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCorreo.frame, duration: tiempo)
            }
            break
        case 2:
            if ((self.boolsError["cedula"]) != nil) && (self.boolsError["cedula"] == true){
                let tte_ced = AMPopTip()
                tte_ced.shouldDismissOnTap = true
                tte_ced.shouldDismissOnTapOutside = true
                tte_ced.showText("No puede ingresar caracteres en el número", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtCedula.frame, duration: tiempo)
            }
            break
        
        case 3:
            if ((self.boolsError["contrasenia"]) != nil) && (self.boolsError["contrasenia"] == true){
                let tte_contra = AMPopTip()
                tte_contra.shouldDismissOnTap = true
                tte_contra.shouldDismissOnTapOutside = true
                tte_contra.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtNuevaContra.frame, duration: tiempo)
            } else if ((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true){
                let tte_contra = AMPopTip()
                tte_contra.shouldDismissOnTap = true
                tte_contra.shouldDismissOnTapOutside = true
                tte_contra.showText("Contraseñas diferentes", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtNuevaContra.frame, duration: tiempo)
            }
            break
        case 4:
            if ((self.boolsError["confContra"]) != nil) && (self.boolsError["confContra"] == true){
                let tte_confcontra = AMPopTip()
                tte_confcontra.shouldDismissOnTap = true
                tte_confcontra.shouldDismissOnTapOutside = true
                tte_confcontra.showText("Mínimo 4 caracteres", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtRepetirContra.frame, duration: tiempo)
            } else if ((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true){
                let tte_confcontra = AMPopTip()
                tte_confcontra.shouldDismissOnTap = true
                tte_confcontra.shouldDismissOnTapOutside = true
                tte_confcontra.showText("Contraseñas diferentes", direction: .Up, maxWidth: 200, inView: self.stackCampos, fromFrame: self.txtRepetirContra.frame, duration: tiempo)
            }
            break
            
        default:
            break
        }
        return true
    }
    
    //MARK: para los on change de los textfields
    func textFieldDidChange(xtextField: UITextField) {
        let xfunciones = funciones()
        switch xtextField.tag {
        case 1:
            if ((self.boolsError["correo"]) != nil) && (self.boolsError["correo"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["correo"] = false
            }
            break
        case 2:
            if ((self.boolsError["cedula"]) != nil) && (self.boolsError["cedula"] == true){
                xfunciones.markValid(xtextField)
                self.boolsError["cedula"] = false
            }
            break
            
        case 3:
            if ( (((self.boolsError["contrasenia"]) != nil) && (self.boolsError["contrasenia"] == true)) || (((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["contrasenia"] = false
                self.boolsError["contraIgual"] = false
            }
            break
        case 4:
            if ( (((self.boolsError["confContra"]) != nil) && (self.boolsError["confContra"] == true)) || (((self.boolsError["contraIgual"]) != nil) && (self.boolsError["contraIgual"] == true)) ){
                xfunciones.markValid(xtextField)
                self.boolsError["confContra"] = false
                self.boolsError["contraIgual"] = false
            }
            break
            
        default:
            break
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
