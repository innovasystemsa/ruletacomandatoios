//
//  MonedasVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 19/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import PureJsonSerializer
import Kingfisher

class MonedasVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var navigationBarItem: UINavigationItem!
    @IBOutlet weak var imgUsuario: UIImageView!
    @IBOutlet weak var lblNombreUsr: UILabel!
    @IBOutlet weak var lblCorreoe: UILabel!
    @IBOutlet weak var lblCedula: UILabel!
    @IBOutlet weak var TableMonedas: UITableView!
    
   
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    //arreglo de premios
    var itemsMonedas = [Moneda]()


    override func viewDidLoad() {
        super.viewDidLoad()
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationBarItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.navigationBarItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
        //imagen en circulo
        self.imgUsuario.hidden = true
        self.imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.height / 2
        self.imgUsuario.clipsToBounds = true
        self.imgUsuario.layer.masksToBounds = true
        self.imgUsuario.layer.borderWidth = 1
        self.imgUsuario.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.TableMonedas.delegate = self
        self.TableMonedas.dataSource = self
        
        self.lblNombreUsr.text = "\(self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.nombre)!.uppercaseString) \(self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.apellido)!.uppercaseString)"
        self.lblCorreoe.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.correo_personal)!
        self.lblCedula.text = "C.I. \(self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.cedula)!)"
        
        self.TableMonedas.separatorStyle = UITableViewCellSeparatorStyle.None

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewDidAppear(animated: Bool) {
        //imagen
        if (self.userDefaults.valueForKey(Constantes.JsonData.usuario.facebook_id) as! String == ""){
            let dataDecoded:NSData = NSData(base64EncodedString: self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.foto)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
            
            let decodedimage:UIImage = UIImage(data: dataDecoded)!
            self.imgUsuario.image = decodedimage
        
        } else {
            
            let imgURL = NSURL(string: self.userDefaults.valueForKey(Constantes.JsonData.datosUsuario.foto) as! String)
            self.imgUsuario.kf_setImageWithURL(imgURL)
        }
        
        //imagen redonda -- checar que se puede hacer para que no haga ese efecto de cambiar de forma
        self.imgUsuario.layer.cornerRadius = (self.imgUsuario.frame.size.height / 2 )
        self.imgUsuario.hidden = false
        self.startActivityIndicator()
        self.cargaDatos()
    }
    
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }

    //MARK: Carga datos del WS
    func cargaDatos(){
        var dtosJson = "{\"metodo\":\"obtenerTotalPuntosUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                }
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los puntos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                    }
                    if (resultado == "error"){
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar obtener los puntos"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.itemsMonedas.removeAll()
                        if (!(json["lista"]?.isNull)!){
                            for item in (json["lista"]?.arrayValue)! {
                                let unaMoneda = Moneda()
                                unaMoneda.nombre = (item["nombre"]?.stringValue)!.uppercaseString
                                unaMoneda.punto_id = (item["punto_id"]?.stringValue)!
                                unaMoneda.usuario_id = (item["usuario_id"]?.stringValue)!
                                if (item["punto_padre_id"]?.stringValue) != nil {
                                    unaMoneda.punto_padre_id = (item["punto_padre_id"]?.stringValue)!
                                } else {
                                    unaMoneda.punto_padre_id = ""
                                }
                                
                                unaMoneda.url_imagen = (item["url_imagen"]?.stringValue)!
                                unaMoneda.total = (item["total"]?.stringValue)!
                                self.itemsMonedas.append(unaMoneda)
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.stopActivityIndicator()
                            self.TableMonedas.reloadData()
                        })
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.itemsMonedas.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("celdaMoneda", forIndexPath: indexPath) as! celdaMoneda
        
        let itemMoneda = self.itemsMonedas[indexPath.row]
        
        let imgURL = NSURL(string: itemMoneda.url_imagen)
        cell.imgMoneda.kf_setImageWithURL(imgURL)
        
        cell.lblCantMonedas.text = itemMoneda.total
        cell.lblNombreMoneda.text = itemMoneda.nombre
        
        /*
        //espacio a la celda
        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height-10))
        whiteRoundedView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 0.7])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
        //espacio a las celdas
        */
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        return screenSize.height / 6
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        _ = indexPath.row
        
    }
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

}

class  Moneda {
    var nombre = ""
    var punto_id = ""
    var usuario_id = ""
    var punto_padre_id = ""
    var url_imagen = ""
    var total = ""
}
