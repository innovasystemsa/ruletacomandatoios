//
//  datosUsuario.swift
//  
//
//  Created by Leonel Sánchez on 18/02/17.
//
//

import Foundation
import UIKit

class datosUsuario {
    var nombre:String = ""
    var apellidos: String = ""
    var cedula: String = ""
    var correo: String = ""
    var direccion_trabajo: String = ""
    var fecha_nac : String = ""
    var lugar_residencia: String = ""
    var telefono: String = ""
    var celular: String = ""
    var contrasenia:String = ""
    var foto: UIImage!
}