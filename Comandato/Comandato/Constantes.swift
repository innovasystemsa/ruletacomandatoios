//
//  Constantes.swift
//  Comandato
//
//  Created by Leonel Sánchez on 01/02/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import Foundation
struct Constantes {
    static let IDgotoLogin = "NavCLogin"
    struct WSData {
        static let username = "comandato"
        static let password = "rulet42016"
        //pruebas
        //static let urlRest = "http://52.27.202.153:83/RuletaComandato/servicios.php"
        //produccion
        static let urlRest = "http://ruleta.comandato.com/RuletaComandato/servicios.php"
        static let metodoHTTP = "POST"
    }
    
    struct JsonData {
        struct base {
            static let usrLogueado = "usrLogueado"
            static let baseNumero = "baseNumero"
            static let cantMonedas = "cantMonedas"
            static let intentos = "intentos"
        }
        
        struct usuario {
            static let email = "email"
            static let estado_id = "estado_id"
            static let facebook_id = "facebook_id"
            static let fecha_creacion = "fecha_creacion"
            static let password = "password"
            static let remember_token = "remember_token"
            static let tipo_usuario_id = "tipo_usuario_id"
            static let usuario_id = "usuario_id"
            static let token = "token"
            
        }
        
        struct datosUsuario {
            static let apellido = "apellido"
            static let cedula = "cedula"
            static let celular = "celular"
            static let correo_personal = "correo_personal"
            static let dato_personal_id = "dato_personal_id"
            static let direccion_casa = "direccion_casa"
            static let direccion_trabajo = "direccion_trabajo"
            static let fecha_creacion = "fecha_creacion"
            static let fecha_modificacion = "fecha_modificacion"
            static let fecha_nacimiento = "fecha_nacimiento"
            static let foto = "foto"
            static let nombre = "nombre"
            static let telefono = "telefono"
        }
        
        struct datosComandato {
            static let comandato_cupo = "comandato_cupo"
            static let comandato_cliente = "comandato_cliente"
        }
        
        struct nivelUsuario {
            static let nivel_id = "nivel_id"
            static let nivel_usuario_id = "nivel_usuario_id"
        }
        
        struct totalPuntos {
            static let nombre_oro = "Oro"
            static let nombre_plata = "Plata"
            static let nombre_bronce = "Bronce"
            static let nombre_rubi = "Rubi"
            static let cantidad_padre = "cantidad_padre"
            static let usuario_id = "usuario_id"
            static let punto_id = "punto_id"
            static let punto_padre_id = "punto_padre_id"
            static let total = "total"
            static let url_imagen = "url_imagen"
        }
    }
    
}
