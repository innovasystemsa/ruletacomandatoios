//
//  CeldaMonedaCV.swift
//  Comandato
//
//  Created by Leonel Sánchez on 23/02/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class CeldaMonedaCV: UICollectionViewCell {
    @IBOutlet weak var imgMoneda: UIImageView!
    @IBOutlet weak var lblCantidadMonedas: UILabel!
    
}
