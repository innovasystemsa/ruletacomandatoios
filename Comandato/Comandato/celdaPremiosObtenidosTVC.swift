//
//  celdaPremiosObtenidosTVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 30/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class celdaPremiosObtenidosTVC: UITableViewCell {
    @IBOutlet weak var imgPremio: UIImageView!
    @IBOutlet weak var lblPremio: UILabel!
    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblNivel: UILabel!
    @IBOutlet weak var lblCantMonedas: UILabel!
    @IBOutlet weak var imgMonedas: UIImageView!
     @IBOutlet weak var btnRetirarPremio: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
