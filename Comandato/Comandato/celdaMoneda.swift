//
//  celdaMoneda.swift
//  Comandato
//
//  Created by Leonel Sánchez on 21/02/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class celdaMoneda: UITableViewCell {
    @IBOutlet weak var imgMoneda: UIImageView!
    @IBOutlet weak var lblCantMonedas: UILabel!
    @IBOutlet weak var lblNombreMoneda: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
