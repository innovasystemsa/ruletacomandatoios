//
//  PPrincipalVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 16/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import PureJsonSerializer
import AMPopTip
import SwiftyJSON

class PPrincipalVC: UIViewController {
    @IBOutlet weak var navigationBarItem: UINavigationItem!
    @IBOutlet weak var viewFondo: UIImageView!
    @IBOutlet weak var jugarContainer: UIView!
    @IBOutlet weak var premiosContainer: UIView!
    @IBOutlet weak var monedasContainer: UIView!
    @IBOutlet weak var escribenosContainer: UIView!
    @IBOutlet weak var puntosContainer: UIView! //tiendas
    @IBOutlet weak var bonificacionesContainer: UIView!
    @IBOutlet weak var imgUsuario: UIImageView!
    @IBOutlet weak var imgPerfil: UIImageView!
    @IBOutlet weak var lblPerfil: UILabel!
    @IBOutlet weak var viewBottomLeft: UIView!
    @IBOutlet weak var viewBottomRight: UIView!
    @IBOutlet weak var viewContainerCabecera: UIView!
    @IBOutlet weak var lblJugar: LabelWithAdaptiveTextHeight!
    @IBOutlet weak var lblPremios: LabelWithAdaptiveTextHeight!
    @IBOutlet weak var lblTxtNivel: LabelWithAdaptiveTextHeight!
    @IBOutlet weak var lblTxtIntentos: LabelWithAdaptiveTextHeight!
    @IBOutlet weak var containerInstrucciones: UIView!
    @IBOutlet weak var lblInstrucciones: LabelWithAdaptiveTextHeight!
    
    //campos variables
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblCorreo: UILabel!
    @IBOutlet weak var lblNivel: UILabel!
    @IBOutlet weak var lblIntentos: UILabel!
    @IBOutlet weak var lblCupo: UILabel!
    @IBOutlet weak var regalosCountContainer: UIView!
    @IBOutlet weak var lblCountContainer: UILabel!
    
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //para los tooltips de nivel e intento func: toolTipNivel, toolTipIntentos
    var ttNivel = false
    var ttIntentos = false
    
    //para instrucciones
    var backTransparente = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        navigationBarItem.titleView = bannerImgView
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        let btnCerrarSesion = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
        var imgControl = UIImage(named: "control")
        imgControl = imgControl?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        let btnControl = UIBarButtonItem(image: imgControl, style: .Plain, target: self, action: #selector(muestraInstrucciones))
        self.navigationBarItem.rightBarButtonItems = [btnCerrarSesion, btnControl]
        
        //imagen compartir
        var imgCompartir = UIImage(named: "compartir26")
        imgCompartir = imgCompartir?.imageWithRenderingMode(.AlwaysOriginal)
        let btnCompartir = UIBarButtonItem(image: imgCompartir, style: .Plain, target: self, action: #selector(shareThis))
        
        //padding
        let paddingLeft = UIBarButtonItem(customView: UIView(frame: CGRectMake(0, 0, 30, 20)))
        
        self.navigationBarItem.leftBarButtonItems = [btnCompartir, paddingLeft]
        
        //asignar el fondo al viewImage y enviar atras
        self.viewFondo.image = UIImage(named: "backImgPrincipal")
        self.view.sendSubviewToBack(viewFondo)
        
        //Redondear bordes de views
        self.jugarContainer.layer.cornerRadius = 16.0
        self.jugarContainer.clipsToBounds = true
        
        self.premiosContainer.layer.cornerRadius = 16.0
        self.premiosContainer.clipsToBounds = true
        
        self.monedasContainer.layer.cornerRadius = 16.0
        self.monedasContainer.clipsToBounds = true
        
        self.escribenosContainer.layer.cornerRadius = 10.0
        self.escribenosContainer.clipsToBounds = true
        
        self.puntosContainer.layer.cornerRadius = 10.0
        self.puntosContainer.clipsToBounds = true
        
        self.bonificacionesContainer.layer.cornerRadius = 10.0
        self.bonificacionesContainer.clipsToBounds = true
        
        //MARK: Segues
        
        //escribenos
        let touchEscribenos = UITapGestureRecognizer(target: self, action: #selector(seguePrincipalToConsultas) )
        self.escribenosContainer.addGestureRecognizer(touchEscribenos)
        
        //perfil
        let touchPerfil = UITapGestureRecognizer(target: self, action: #selector(seguePrincipalToPerfil))
        self.viewBottomLeft.addGestureRecognizer(touchPerfil)
        
        
        //monedas
        let touchMonedas = UITapGestureRecognizer(target: self, action: #selector(seguePrincipalToMonedas))
        self.monedasContainer.addGestureRecognizer(touchMonedas)
        
        //tiendas
        let touchTiendas = UITapGestureRecognizer(target: self, action: #selector(seguePrincipalToTiendas))
        self.puntosContainer.addGestureRecognizer(touchTiendas)
        
        //regalos
        let touchRegalos = UITapGestureRecognizer(target: self, action: #selector(seguePrincipalToRegalos))
        self.bonificacionesContainer.addGestureRecognizer(touchRegalos)
        
        //premios
        let touchPremios = UITapGestureRecognizer(target: self, action: #selector(seguePrincipalToPremios))
        self.premiosContainer.addGestureRecognizer(touchPremios)
        
        //subir nivel
        let touchSubirNivel = UITapGestureRecognizer(target: self, action: #selector(self.confSubirNivel))
        self.viewBottomRight.addGestureRecognizer(touchSubirNivel)
        
        //jugar
        let touchJugar = UITapGestureRecognizer(target: self, action: #selector(self.seguePrincipalToParticipar(_:)))
        self.jugarContainer.addGestureRecognizer(touchJugar)
        
        //tooltip intentos nivel
        self.lblNivel.userInteractionEnabled = true
        self.lblTxtNivel.userInteractionEnabled = true
        self.lblNivel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toolTipNivel)))
        self.lblTxtNivel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toolTipNivel)))
        self.lblIntentos.userInteractionEnabled = true
        self.lblTxtIntentos.userInteractionEnabled = true
        self.lblIntentos.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toolTipIntentos)))
        self.lblTxtIntentos.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toolTipIntentos)))
        
        //texto de instrucciones
       // let paragraphStyle = NSMutableParagraphStyle()
       // paragraphStyle.alignment = NSTextAlignment.Justified
        
        let textoInst = "1.- DEPENDIENDO DE LOS INTENTOS QUE TENGAS PODRÁS PARTICIPAR EN EL JUEGO\n2.- SELECCIONAR LA OPCIÓN JUGAR\n3.- AL DAR CLICK EN JUGAR GANARÁS UNA CANTIDAD DE MONEDAS AL AZAR\n4.- DEPENDIENDO DEL MONTO DE MONEDAS PODRÁS CANJEAR LOS DIFERENTES PREMIOS DISPONIBLES Y PROMOCIONES"
        
        self.lblInstrucciones.text = textoInst
        //let attrString = NSAttributedString(string: textoInst, attributes: [ NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(float: 0)])
        
        
        //self.lblInstrucciones.attributedText = attrString
        //self.lblInstrucciones.adjustsFontSizeToFitWidth = true
        //self.lblInstrucciones.minimumScaleFactor = 0.5
        //self.lblInstrucciones.sizeToFit()
        
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        //imagen
        if (self.userDefaults.stringForKey(Constantes.JsonData.usuario.facebook_id) == ""){
            let dataDecoded:NSData = NSData(base64EncodedString: self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.foto)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
            
            let decodedimage:UIImage = UIImage(data: dataDecoded)!
            self.imgUsuario.image = decodedimage
        } else {
            let imgURL = NSURL(string: self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.foto)!)
            self.imgUsuario.kf_setImageWithURL(imgURL)
        }
        
        //imagen redonda
        self.imgUsuario.layer.cornerRadius = (self.imgUsuario.frame.size.height / 2 )
        self.imgUsuario.clipsToBounds = true
        self.imgUsuario.layer.masksToBounds = true
        self.imgUsuario.layer.borderWidth = 0
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //imagen redonda -- checar que se puede hacer para que no haga ese efecto de cambiar de forma
        self.imgUsuario.layer.cornerRadius = (self.imgUsuario.frame.size.height / 2 )
        
        //asignar datos a campos
        self.lblNombre.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.nombre)?.uppercaseString
        self.lblCorreo.text = self.userDefaults.stringForKey(Constantes.JsonData.datosUsuario.correo_personal)
        self.lblCupo.text = "Cupo: " + self.userDefaults.stringForKey(Constantes.JsonData.datosComandato.comandato_cupo)!
        
        self.lblNivel.text = self.userDefaults.stringForKey(Constantes.JsonData.nivelUsuario.nivel_id)
        self.lblIntentos.text = self.userDefaults.stringForKey(Constantes.JsonData.base.intentos)
        
        self.getNivelUsuario()
        
        self.lblCorreo.font = self.lblCupo.font
    }

    
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func muestraInstrucciones(){
        if (self.containerInstrucciones.hidden){
            let screenSize: CGRect = UIScreen.mainScreen().bounds
            backTransparente = UIView(frame: CGRectMake(0, 0, 0, 0))
            backTransparente.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
            backTransparente.center = self.view.center
            // Change background color and alpha channel here
            backTransparente.backgroundColor = UIColor.blackColor()
            backTransparente.clipsToBounds = true
            backTransparente.alpha = 0.5
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.ocultaInstrucciones))
            backTransparente.addGestureRecognizer(tap)
            view.addSubview(backTransparente)
            
            self.containerInstrucciones.hidden = false
            self.view.bringSubviewToFront(self.containerInstrucciones)
        } else {
            self.ocultaInstrucciones()
        }
    }
    
    func ocultaInstrucciones(){
        backTransparente.removeFromSuperview()
        self.containerInstrucciones.hidden = true
    }
    
    //MARK: FUNCIONES SEGUES
    
    func seguePrincipalToConsultas(sender: UITapGestureRecognizer){
        
        self.performSegueWithIdentifier("PrincipalToConsultaSegue", sender: self)
    }
    
    func seguePrincipalToPerfil(sender: UITapGestureRecognizer){
        self.performSegueWithIdentifier("PrincipalToPerfilSegue", sender: self)
    }
    
    func seguePrincipalToMonedas(sender: UITapGestureRecognizer){
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        self.performSegueWithIdentifier("PrincipalToMonedasSegue", sender: self)
    }
    
    func seguePrincipalToTiendas(sender: UITapGestureRecognizer){
        self.performSegueWithIdentifier("PrincipalToTiendasSegue", sender: self)
    }
    
    func seguePrincipalToRegalos(sender: UITapGestureRecognizer){
        self.performSegueWithIdentifier("PrincipalToRegalosSegue", sender: self)
    }
    
    func seguePrincipalToPremios(sender: UITapGestureRecognizer){
        self.performSegueWithIdentifier("PrincipalToPremiosSegue", sender: self)
    }
    
    func seguePrincipalToParticipar(sender: UITapGestureRecognizer){
        self.startActivityIndicator()
        let id_usr = self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id)
        
        var dtosJson = "{\"metodo\":\"obtenerIntentosUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(id_usr!)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar consultar los intentos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: false, completion: nil)
                }
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar consultar los intentos"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        
                        //poner alter error
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                    }
                        
                    else if (resultado == "ok"){
                        
                        
                        if (!(json["lista"]?.isNull)!){
                            if (json["lista"]?[0]?["intentos"] != nil){
                                let intentos = json["lista"]![0]!["intentos"]!.stringValue
                                dispatch_async(dispatch_get_main_queue()){
                                    self.stopActivityIndicator()
                                    self.userDefaults.setObject(intentos, forKey: Constantes.JsonData.base.intentos)
                                    self.lblIntentos.text = intentos
                                    if Int(intentos!)! > 0 {
                                        self.performSegueWithIdentifier("PrincipalToParticiparSegue", sender: self)
                                    } else {
                                       // NSOperationQueue.mainQueue().addOperationWithBlock {
                                            
                                            let alertaError = UIAlertController(title: "", message: "No tienes intentos para participar", preferredStyle: .ActionSheet)
                                            self.presentViewController(alertaError, animated: true, completion: nil)
                                            
                                            let delay = 1.5 * Double(NSEC_PER_SEC)
                                            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                                            dispatch_after(time, dispatch_get_main_queue()) { () -> Void in
                                                alertaError.dismissViewControllerAnimated(true, completion: nil)
                                            }
                                        //}
                                    }
                                }
                            }
                        } else {
                            NSLog("al tratar de leer los intentos de usuario, se ejecuta correcto, la lista llega null")
                        }
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    //MARK: alerta para subir nivel usuario
    func confSubirNivel(){
        let subirNivelAlert = UIAlertController(title: "Subir de Nivel", message: "¿Está seguro que desea subir de nivel?", preferredStyle: .Alert)
        
        subirNivelAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            //validar el json
            let dtosJson = self.generaJsonDatosUsuario()
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                return
            }
            self.startActivityIndicator()
            self.subirNivelUsuario(dtosJson)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        subirNivelAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(subirNivelAlert, animated: true, completion: nil)
    }
    
    //MARK: funcion subir nivel de usuario
    func subirNivelUsuario(dtosJson: String){
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar subir de nivel", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: false, completion: nil)
                }
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar subir de nivel"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        
                        //poner alter error
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                    }
                        
                    else if (resultado == "ok"){
                        
                        //guardar datos de usuario
                        if (!(json["lista"]?.isNull)!){
                            if (json["lista"]?[0]?["nivel_id"] != nil){
                                let elnivel = json["lista"]![0]!["nivel_id"]!.stringValue
                                self.userDefaults.setObject(elnivel, forKey: Constantes.JsonData.nivelUsuario.nivel_id)
                                self.lblNivel.text = elnivel
                            }
                        } else {
                            NSLog("Al tratar de subir el nivel de usuario, se ejecuto correcto, la lista llego null")
                        }
                        
                        var texto = json["texto"]?.stringValue ?? "Se ha incrementado el nivel correctamente"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaOK = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        let alertaOKAccion = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaOK.addAction(alertaOKAccion)
                        
                        //poner alter ok
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaOK, animated: true, completion: nil)
                        }
                        
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    func generaJsonDatosUsuario()-> String{
        //eljson
        let paraJson: [String: AnyObject] = [
            "metodo": "subirNivel",
            "parametros": [
                [
                    "nombre": Constantes.JsonData.usuario.usuario_id,
                    "valor": self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String
                ]
            ]
        ]
        
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            return jsonString
        } else {
            return ""
        }

    }
    
    func getNivelUsuario(){
        self.startActivityIndicator()
        let id_usr = self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id)
        
        var dtosJson = "{\"metodo\":\"obtenerNivelUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(id_usr!)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar consultar el nivel", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: false, completion: nil)
                }
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar consultar el nivel"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        
                        //poner alter error
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                    }
                        
                    else if (resultado == "ok"){
                        
                        if (!(json["lista"]?.isNull)!){
                            if (json["lista"]?[0]?["nivel_id"] != nil){
                                let elnivel = json["lista"]![0]!["nivel_id"]!.stringValue
                                dispatch_async(dispatch_get_main_queue()){
                                    self.userDefaults.setObject(elnivel, forKey: Constantes.JsonData.nivelUsuario.nivel_id)
                                self.lblNivel.text = elnivel
                                }
                            }
                        }  else {
                            NSLog("al tratar de leer el nivel de usuario, se ejecuta correcto, la lista llega null")
                        }
                        self.getIntentosUsuario()
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    func getIntentosUsuario(){
        
        let id_usr = self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id)
        
        var dtosJson = "{\"metodo\":\"obtenerIntentosUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(id_usr!)\""
        dtosJson += "}]"
        dtosJson += "}"
        
      
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar consultar los intentos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: false, completion: nil)
                }
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar consultar los intentos"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        
                        //poner alter error
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                    }
                        
                    else if (resultado == "ok"){
                        
                        
                        if (!(json["lista"]?.isNull)!){
                            if (json["lista"]?[0]?["intentos"] != nil){
                                let intentos = json["lista"]![0]!["intentos"]!.stringValue
                                dispatch_async(dispatch_get_main_queue()){
                                    self.userDefaults.setObject(intentos, forKey: Constantes.JsonData.base.intentos)
                                    self.lblIntentos.text = intentos
                                }
                            }
                        } else {
                            NSLog("al tratar de leer los intentos de usuario, se ejecuta correcto, la lista llega null")
                        }
                        dispatch_async(dispatch_get_main_queue()){
                            self.getRegalosUsuario()
                        }
                        
                        
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    func getRegalosUsuario(){
        
        let id_usr = self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id)
        
        var dtosJson = "{\"metodo\":\"obtenerEventoUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(id_usr!)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar consultar los regalos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: false, completion: nil)
                }
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar consultar los regalos"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        //let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        //}
                        //alertaError.addAction(okActionError)
                        
                        if (!(json["lista"]?.isNull)!){
                            NSOperationQueue.mainQueue().addOperationWithBlock {
                                let cant_regalos = json["lista"]!.arrayValue?.count
                                if cant_regalos == 0 {
                                    self.regalosCountContainer.hidden = true
                                }
                                self.stopActivityIndicator()
                                self.presentViewController(alertaError, animated: true, completion: nil)
                                
                                let delay = 2 * Double(NSEC_PER_SEC)
                                let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                                dispatch_after(time, dispatch_get_main_queue()) { () -> Void in
                                    alertaError.dismissViewControllerAnimated(true, completion: nil)
                                }
                            }
                        } else {
                            NSOperationQueue.mainQueue().addOperationWithBlock {
                                self.stopActivityIndicator()
                            }
                            NSLog("al tratar de consultar los regalos de usuario, se ejecuta correcto, la lista llega null")
                        }
                    }
                        
                    else if (resultado == "ok"){
                        
                        if (!(json["lista"]?.isNull)!){
                            NSOperationQueue.mainQueue().addOperationWithBlock {
                                self.regalosCountContainer.layer.cornerRadius = self.regalosCountContainer.frame.size.width/2
                                self.regalosCountContainer.clipsToBounds = true
                                let cant_regalos = json["lista"]!.arrayValue?.count
                                self.lblCountContainer.text = String(cant_regalos!)
                                self.regalosCountContainer.hidden = false
                                self.stopActivityIndicator()
                            }
                        } else {
                            NSOperationQueue.mainQueue().addOperationWithBlock {
                                self.stopActivityIndicator()
                            }
                            NSLog("al tratar de consultar los regalos de usuario, se ejecuta correcto, la lista llega null")
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func toolTipNivel(){
        if !self.ttNivel {
            let ttnivel = AMPopTip()
            ttnivel.shouldDismissOnTap = true
            ttnivel.shouldDismissOnTapOutside = true
            ttnivel.popoverColor = UIColor.blackColor()
            ttnivel.showText("Nivel Actual", direction: .Down, maxWidth: 200, inView: self.viewContainerCabecera, fromFrame: self.lblTxtNivel.frame, duration: 2)
            self.ttNivel = true
            ttnivel.dismissHandler = {
                self.ttNivel = false
            }
        }
    }
    
    func toolTipIntentos(){
        if !self.ttIntentos {
            let ttintentos = AMPopTip()
            ttintentos.shouldDismissOnTap = true
            ttintentos.shouldDismissOnTapOutside = true
            ttintentos.popoverColor = UIColor.blackColor()
            ttintentos.showText("Tienes este número de intentos disponibles para participar", direction: .Down, maxWidth: 300, inView: self.viewContainerCabecera, fromFrame: self.lblTxtIntentos.frame, duration: 2)
            self.ttIntentos = true
            ttintentos.dismissHandler = {
                self.ttIntentos = false
            }
        }
    }
    
    func shareThis(sender: AnyObject) {
        
        let firstActivityItem = "Bájate la nueva aplicación móvil de Comandato, para que te diviertas ganando muchos premios"
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.barButtonItem = (sender as! UIBarButtonItem)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityTypePostToWeibo,
            UIActivityTypePrint,
            UIActivityTypeAssignToContact,
            UIActivityTypeSaveToCameraRoll,
            UIActivityTypeAddToReadingList,
            UIActivityTypePostToFlickr,
            UIActivityTypePostToVimeo,
            UIActivityTypePostToTencentWeibo,
            UIActivityTypeAirDrop,
            UIActivityTypeOpenInIBooks,
            UIActivityTypeAssignToContact
        ]
        
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            if !success{
                
                return
            }
            
            print("La actividad seleccionada: \(activity)")
            
            //hacer algo con la actividad para obtener un valor para enviar el WS
            //let actividad = activity
            //llamar al webservice
            //self.getPremioShare(actividad)
            if activity == UIActivityTypePostToTwitter {
                print("twitter")
            }
            
            if activity == UIActivityTypeMail {
                print("mail")
            }
            
        }
        
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    func getPremioShare(tipo: String){
        self.startActivityIndicator()
        let id_usr = self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id)
        
        var dtosJson = "{\"metodo\":\"eventoCompartir\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(id_usr!)\""
        dtosJson += "},{"
        dtosJson += "\"nombre\": \"referencia\","
        dtosJson += "\"valor\": \"\(tipo)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar consultar los intentos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                    self.presentViewController(alertaError, animated: false, completion: nil)
                }
            } else {
                do {
                    //deserializar los datos
                    let json = JSON(data: data!)
                    let resultado = json["resultado"].stringValue ?? ""
                    
                    if (resultado == "error"){
                        
                        
                        var texto = json["texto"].stringValue ?? "Ocurrio un error al intentar obtener regalo por compartir"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .ActionSheet)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        
                        //poner alter error
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                    }
                        
                    else if (resultado == "ok"){
                        
                        
                        if (!(json["lista"].exists())){
                            dispatch_async(dispatch_get_main_queue()){
                                self.stopActivityIndicator()
                                let texto = json["lista"].stringValue ?? "¡Has ganado un regalo por haber compartido la aplicación a tus amigos!"
                                let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }
                            
                        } else {
                            NSLog("al tratar de leer los intentos de usuario, se ejecuta correcto, la lista llega null")
                        }
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }


    
    // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
    }
    */
    

} //fin de clase
