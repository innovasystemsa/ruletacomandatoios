//
//  celdaPremiosPromoTVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 30/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class celdaPremiosPromoTVC: UITableViewCell {
    @IBOutlet weak var imgPremio: UIImageView!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblValidoHasta: UILabel!
    @IBOutlet weak var btnCanjear: UIButton!
    @IBOutlet weak var lblNivel: UILabel!
    @IBOutlet weak var lblCosto: UILabel!
    @IBOutlet weak var lblDescuento: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var imgMonedas: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
