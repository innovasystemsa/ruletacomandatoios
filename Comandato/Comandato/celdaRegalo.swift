//
//  celdaRegalo.swift
//  Comandato
//
//  Created by Leonel Sánchez on 25/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit

class celdaRegalo: UITableViewCell {
    @IBOutlet weak var imgRegalo: UIImageView!
    @IBOutlet weak var lblNombreRegalo: UILabel!
    @IBOutlet weak var lblValidoHasta: UILabel!
    @IBOutlet weak var btnReclamar: UIButton!
    @IBOutlet weak var lblCantidad: UILabel!
    @IBOutlet weak var lblLogro: UILabel!
    @IBOutlet weak var imgMoneda: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
