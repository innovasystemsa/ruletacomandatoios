//
//  RegalosVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 25/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
//import PureJsonSerializer
import SwiftyJSON

class RegalosVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let cellIdentifier = "celdaRegalo"
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    //arreglo de premios
    var itemsRegalos = [Regalo]()
    
    var btnsReclamar = [UIButton]()

    
    @IBOutlet weak var regalosTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.regalosTable.delegate = self
        self.regalosTable.dataSource = self
        
        //ocultar celdas vacias
        self.regalosTable.tableFooterView = UIView(frame: CGRectZero)
        
        let imgFondo = UIImage(named: "backDragon")
        let imgViewFondo = UIImageView(image: imgFondo)
        
        self.regalosTable.backgroundView = imgViewFondo
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.startActivityIndicator()
        self.cargaDatos()
    }
    
    //MARK: Carga datos del WS
    func cargaDatos(){
        var dtosJson = "{\"metodo\":\"obtenerEventoUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                }
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los eventos de usuario", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                    
                
            } else {
                do {
                    //deserializar los datos
                    let json = JSON(data: data!)
                    let resultado = json["resultado"].stringValue ?? ""
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                    }
                    if (resultado == "error"){
                        
                        var texto = json["texto"].stringValue ?? "Ocurrio un error al intentar obtener los eventos de usuario"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                           self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.itemsRegalos.removeAll()
                        self.btnsReclamar.removeAll()
                        if (json["lista"].exists()){
                            for item in (json["lista"].arrayValue) {
                                let unRegalo = Regalo()
                                
                                unRegalo.cantidad = (item["cantidad"].stringValue) //cantidad de monedas
                                
                                if (item["fecha_reclamado"].exists()){
                                    unRegalo.fecha_reclamado = (item["fecha_reclamado"].stringValue)
                                }
                                
                                if (item["referencia"].exists()){
                                    unRegalo.referencia = (item["referencia"].stringValue)
                                }
                                
                                if (item["fecha_inicio"].exists()){
                                    unRegalo.fecha_inicio = (item["fecha_inicio"].stringValue)
                                }
                                
                                if (item["nombreEventoComandato"].exists()){
                                    unRegalo.nombreEventoComandato = (item["nombreEventoComandato"].stringValue)
                                }
                                
                                if (item["evento_comandato_id"].exists()){
                                    unRegalo.evento_comandato_id = (item["evento_comandato_id"].stringValue)
                                }
                                if (item["usuario_id"].exists()){
                                    unRegalo.usuario_id = (item["usuario_id"].stringValue)
                                }
                                if (item["fecha_fin"].exists()){
                                    unRegalo.fecha_fin = (item["fecha_fin"].stringValue)
                                }
                                if (item["nombrePunto"].exists()){
                                    unRegalo.nombrePunto = (item["nombrePunto"].stringValue)
                                }
                                if (item["fecha_creacion"].exists()){
                                    unRegalo.fecha_creacion = (item["fecha_creacion"].stringValue)
                                }
                                if (item["evento_usuario_id"].exists()){
                                    unRegalo.evento_usuario_id = (item["evento_usuario_id"].stringValue)
                                }
                                if (item["estado_id"].exists()){
                                    unRegalo.estado_id = (item["estado_id"].stringValue)
                                }
                                if (item["punto_id"].exists()){
                                    unRegalo.punto_id = (item["punto_id"].stringValue)
                                }
                                if (item["nombreEvento"].exists()){
                                    unRegalo.nombreEvento = (item["nombreEvento"].stringValue)
                                }
                                if (item["url_imagen"].exists()){
                                    unRegalo.url_imagen = (item["url_imagen"].stringValue)
                                }
                                self.itemsRegalos.append(unRegalo)
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.stopActivityIndicator()
                                self.regalosTable.reloadData()
                            })
                        } else {
                            NSLog("Se consulto correctamente los regalos, pero la lista llego null")
                        }
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                }
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }

    
    
    //para table view
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsRegalos.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier, forIndexPath: indexPath) as! celdaRegalo
        
        let elRegalo = self.itemsRegalos[indexPath.row]
        
        
        if (elRegalo.url_imagen != ""){
            let imgURL = NSURL(string: elRegalo.url_imagen)
            cell.imgMoneda.kf_setImageWithURL(imgURL)
        }
        
        
        cell.lblNombreRegalo.text = elRegalo.nombreEvento
        cell.lblValidoHasta.text = "Válido hasta: \(elRegalo.fecha_fin)"
        cell.lblCantidad.text = elRegalo.cantidad
        cell.lblLogro.text = elRegalo.nombreEventoComandato
        cell.lblLogro.sizeToFit()
        cell.lblLogro.layoutSubviews()
        cell.lblLogro.layoutIfNeeded()
        
        cell.btnReclamar.addTarget(self, action: #selector(self.canjearPremio(_:)), forControlEvents: .TouchUpInside)
        self.btnsReclamar.append(cell.btnReclamar)
        
        //espacio a la celda
        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height-10))
        whiteRoundedView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 0.7])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
        //espacio a las celdas
        
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //
    }
    
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

    //MARK: Canjear premio
    func canjearPremio(sender:UIButton){
        let elIndice = self.btnsReclamar.indexOf(sender)
        if elIndice != nil {
            let idEvento = self.itemsRegalos[elIndice!].evento_usuario_id
            
            let dtosJson = generaJson(idEvento)
            
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                return
            }
            
            self.startActivityIndicator()
            
            //para el envio de datos
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
            
            //envio de datos
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                let httpResponse = response as? NSHTTPURLResponse
                
                if httpResponse?.statusCode != 200 {
                    print(httpResponse?.statusCode)
                }
                
                if ( error != nil ) {
                    print("Localized description error: \(error!.localizedDescription)")
                    
                    let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar canjear el premio", preferredStyle: .Alert)
                    let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                        //No hacer nada por ahora
                    }
                    alertaError.addAction(okActionError)
                    //quitar el loader
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopActivityIndicator()
                        self.presentViewController(alertaError, animated: false, completion: nil)
                    }
                    
                } else {
                    do {
                        //deserializar los datos
                        let json = JSON(data: data!)
                        let resultado = json["resultado"].stringValue ?? ""
                        
                        if (resultado == "error"){
                            
                            
                            var texto = json["texto"].stringValue ?? "Ocurrio un error al intentar canjear el premio"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            
                            //poner alter error
                            dispatch_async(dispatch_get_main_queue()){
                                self.stopActivityIndicator()
                                self.presentViewController(alertaError, animated: true, completion: nil)
                                self.cargaDatos()
                            }
                        }
                            
                        else if (resultado == "ok"){
                            
                            
                            var texto = json["texto"].stringValue ?? "Se ha reclamado la bonificación con éxito"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaOK = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let alertaOKAccion = UIAlertAction(title: "OK", style: .Cancel) { action in
                                self.cargaDatos()
                            }
                            alertaOK.addAction(alertaOKAccion)
                            
                            //poner alter ok
                            dispatch_async(dispatch_get_main_queue()){
                                self.stopActivityIndicator()
                                self.presentViewController(alertaOK, animated: true, completion: nil)
                            }
                            
                            
                        } else {
                            //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                            //enviar alerta de error
                            print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                        }
                        
                    }
                }
            } //hasta aqui el data task request
            
            task.resume()
            
            //hasta aca envio de datos
        }
    }
    
    //MARK: para generar el json
    func generaJson(id_premio:String)->String{
        let paraJson: [String: AnyObject] = [
            "metodo": "reclamarEventoUsuario",
            "parametros": [
                [
                    "nombre":"evento_usuario_id",
                    "valor": id_premio
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            
            return jsonString
        } else {
            return ""
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

} //fin de clase


class Regalo {
    var cantidad = ""
    var fecha_reclamado = ""
    var referencia = ""
    var fecha_inicio = ""
    var nombreEventoComandato = ""
    var evento_comandato_id = ""
    var usuario_id = ""
    var fecha_fin = ""
    var nombrePunto = ""
    var fecha_creacion = ""
    var evento_usuario_id = ""
    var estado_id = ""
    var punto_id = ""
    var nombreEvento = ""
    var url_imagen = ""
}
