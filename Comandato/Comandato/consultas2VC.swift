//
//  consultas2VC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 24/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import PureJsonSerializer
import AMPopTip

class consultas2VC: UIViewController, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var txtPicker: UITextField!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblApellidos: UILabel!
    @IBOutlet weak var lblTelefono: UILabel!
    @IBOutlet weak var lblCorreoe: UILabel!
    @IBOutlet weak var txtMensaje: UITextView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var btnEnviar: UIButton!
    
    let textViewPlaceHolder : String = "Mensaje"
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var dic_tipoMensaje = [String: String]()
    var tipoMensaje = [String]()
    var tipoMensajeSeleccionado = ""
    
    var mensajeEnviar = ""
    
    var mensajeError = false
    
    let opcionpicker : UIPickerView = UIPickerView()
    
    let userDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setear el logo en la navigation bar
        let bannerImg = UIImage(named: "logo_banner")
        let bannerImgView = UIImageView(image: bannerImg)
        bannerImgView.frame = CGRectMake(0, 0, 179, 13)
        bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
        self.navigationItem.titleView = bannerImgView
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //boton para cerrar sesion
        var imgCerrarSesion = UIImage(named: "cerrarsesion70")
        imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(cerrarSession))
        
        //picker para las opciones, eventualmente se cargaran por webservice, por ahora fijo
        
        opcionpicker.delegate = self
        
        
        //configuracion a controles
        self.txtPicker.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.txtPicker.layer.borderWidth = 0.5
        self.txtPicker.inputView = opcionpicker
        self.txtPicker.tintColor = UIColor.clearColor()
        self.lblNombre.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.lblNombre.layer.borderWidth = 0.5
        self.lblApellidos.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.lblApellidos.layer.borderWidth = 0.5
        self.lblTelefono.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.lblTelefono.layer.borderWidth = 0.5
        self.lblCorreoe.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.lblCorreoe.layer.borderWidth = 0.5
        
        //para textview
        self.txtMensaje.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.txtMensaje.layer.borderWidth = 0.5
        self.txtMensaje.text = self.textViewPlaceHolder
        self.txtMensaje.textColor = UIColor.lightGrayColor()
        self.txtMensaje.delegate = self
        
        
        //carga opciones
        self.cargaTiposMensaje()
        
        //botones para picker view
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 100/255, blue: 238/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.donePicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(self.donePicker(_:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        self.txtPicker.inputAccessoryView = toolBar
        
        self.btnEnviar.addTarget(self, action: #selector(self.enviaConsulta), forControlEvents: .TouchUpInside)
        
        //llenar campos
        let espacios = "  "
        self.lblNombre.text = espacios + (self.userDefaults.valueForKey(Constantes.JsonData.datosUsuario.nombre)! as! String)
        self.lblApellidos.text = espacios + (self.userDefaults.valueForKey(Constantes.JsonData.datosUsuario.apellido)! as! String)
        self.lblCorreoe.text = espacios + (self.userDefaults.valueForKey(Constantes.JsonData.datosUsuario.correo_personal)! as! String)
        self.lblTelefono.text = espacios + (self.userDefaults.valueForKey(Constantes.JsonData.datosUsuario.telefono)! as! String)
        
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        //observers para mover la pantalla cuando el teclado aparece o se oculta
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        //Ocultar si esta abierto el teclado
        self.hideKeyboard()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func cargaTiposMensaje(){
        let dtosJson = "{\"metodo\":\"todosTipoMensaje\"}"
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    //alert.dismissViewControllerAnimated(true, completion: nil)
                }
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los tipos de mensaje", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    
                    if (resultado == "error"){
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar obtener los tipos de mensaje"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        self.presentViewController(alertaError, animated: true, completion: nil)
                        
                    }
                        
                    else if (resultado == "ok"){
                        self.tipoMensaje.removeAll()
                        if (!(json["lista"]?.isNull)!){
                            for item in (json["lista"]?.arrayValue)! {
                                let id = item["tipo_mensaje_id"]?.stringValue!
                                let nombre = item["nombre"]?.stringValue!
                                self.dic_tipoMensaje[nombre!] = id
                                self.tipoMensaje.append(nombre!)
                            }
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.txtPicker.text = self.tipoMensaje[0]
                                })
                            })
                        }
                        
                        
                    } else {
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }
    
    //MARK: VALIDA FORMA
    func esValidaLaForma()->Bool{
        //Mensaje
        var error = false
        
        self.mensajeError = false
        
        let xfunciones = funciones()
        
        let mensaje = self.txtMensaje.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        if (mensaje?.characters.count < 10){
            xfunciones.markInvalid(self.txtMensaje)
            xfunciones.shakeView(self.txtMensaje)
            error = true
            let tte_msg = AMPopTip()
            tte_msg.shouldDismissOnTap = true
            tte_msg.shouldDismissOnTapOutside = true
            tte_msg.showText("Mínimo 10 caracteres", direction: .Up, maxWidth: 200, inView: self.viewContainer, fromFrame: self.txtMensaje.frame, duration: 2)
            self.mensajeError = true
        }
        
        
        if (!error){
            //datos validados sin error
            self.mensajeEnviar = mensaje!
        }
        
        return !error
    }
    
    func textViewDidChange(textView: UITextView) { //Handle the text changes here
        if (self.mensajeError){
            self.txtMensaje.layer.borderColor = UIColor.lightGrayColor().CGColor
            self.txtMensaje.layer.borderWidth = 0.5
            self.txtMensaje.textColor = UIColor.lightGrayColor()
        }
    }
    
    //MARK: para generar el json
    func generaJsonDatosUsuario()->String{
        let usuarioid = self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String
        let tipoMensaje = self.dic_tipoMensaje[self.txtPicker.text!]
        let paraJson: [String: AnyObject] = [
            "metodo": "insertarMensaje",
            "parametros": [
                [
                    "nombre":"tipo_mensaje_id",
                    "valor": tipoMensaje!
                ],
                [
                    "nombre":Constantes.JsonData.usuario.usuario_id,
                    "valor": usuarioid
                ],
                [
                    "nombre":"imei",
                    "valor": ""
                ],
                [
                    "nombre":"mensaje",
                    "valor": self.mensajeEnviar
                ]
            ]
        ]
        if (NSJSONSerialization.isValidJSONObject(paraJson)){
            let jsonData = try! NSJSONSerialization.dataWithJSONObject(paraJson, options: NSJSONWritingOptions())
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
            return jsonString
        } else {
            return ""
        }
        
    }

    
    func enviaConsulta(){
        if (self.esValidaLaForma()){
            self.startActivityIndicator()
            let dtosJson = self.generaJsonDatosUsuario()
            if (dtosJson == ""){
                let alertaError = UIAlertController(title: "", message: "A ocurrido un error al enviar los datos", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                self.presentViewController(alertaError, animated: true, completion: nil)
                return
            }
            
            //para el envio de datos
            let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
            let session = NSURLSession.sharedSession()
            
            let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
            let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
            let base64LoginString = loginData.base64EncodedStringWithOptions([])
            
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = Constantes.WSData.metodoHTTP
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
            request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
            
            //envio de datos
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                let httpResponse = response as? NSHTTPURLResponse
                
                if httpResponse?.statusCode != 200 {
                    print(httpResponse?.statusCode)
                }
                
                if ( error != nil ) {
                    print("Localized description error: \(error!.localizedDescription)")
                    
                    let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar enviar consulta", preferredStyle: .Alert)
                    let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                        //No hacer nada por ahora
                    }
                    alertaError.addAction(okActionError)
                    dispatch_async(dispatch_get_main_queue()){
                       self.stopActivityIndicator()
                        self.presentViewController(alertaError, animated: true, completion: nil)
                    }
                    
                } else {
                    do {
                        //deserializar los datos
                        let json = try Json.deserialize(data!)
                        let resultado = json["resultado"]?.stringValue ?? ""
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.stopActivityIndicator()
                        })
                        
                        if (resultado == "error"){
                            var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar enviar consulta"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                //No hacer nada por ahora
                            }
                            alertaError.addAction(okActionError)
                            dispatch_async(dispatch_get_main_queue()){
                                
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }
                            
                        }
                            
                        else if (resultado == "ok"){
                            var texto = json["texto"]?.stringValue ?? "Mensaje enviado correctamente"
                            texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                            let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                            let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                                self.performSegueWithIdentifier("consultasToPrincipalSegue", sender: self)
                            }
                            alertaError.addAction(okActionError)
                            dispatch_async(dispatch_get_main_queue()){
                                self.presentViewController(alertaError, animated: true, completion: nil)
                            }
                        } else {
                            //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                            //enviar alerta de error
                            print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                        }
                        
                    } catch {
                        print("Json serialization failed with error: \(error)")
                    }
                    
                }
            } //hasta aqui el data task request
            
            task.resume()
            
            //hasta aca envio de datos
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    //MARK: Para el placeholder del textview
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        
        self.txtMensaje.textColor = UIColor.blackColor()
        
        if(self.txtMensaje.text == self.textViewPlaceHolder) {
            self.txtMensaje.text = ""
        }
        
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if(textView.text == "") {
            self.txtMensaje.text = self.textViewPlaceHolder
            self.txtMensaje.textColor = UIColor.lightGrayColor()
        }
    }
    
    // hasta aqui para: Para el placeholder del textview

    //MARK: Para ocultar teclado
    /*
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.viewContainer.endEditing(true)
        self.view.endEditing(true)
    }
 */
    //para ocultar teclado
    
    
    //para mover la pantalla cuando se muestra el teclado
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollContainer.contentInset
        contentInset.bottom = keyboardFrame.size.height
        
        self.scrollContainer.contentInset = contentInset
        
        self.scrollContainer.setContentOffset(CGPointMake(0, (keyboardFrame.size.height / 2)), animated: true)
        
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollContainer.contentInset = contentInset
    }
    //para mover la pantalla cuando se muestra el teclado

    //MARK: Metodos para el pickerview
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.tipoMensaje.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.tipoMensaje[row]
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = self.tipoMensaje[row]
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        //pickerLabel.font = UIFont(name: "Arial", size: 15) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tipoMensajeSeleccionado = self.tipoMensaje[row]
    }
    
    func donePicker (sender: UIBarButtonItem){
        if (sender.title! == "OK"){
            self.txtPicker.text = self.tipoMensajeSeleccionado
        }
        
        self.txtPicker.endEditing(true)
        
        
    }
    
    //hasta aca metodos para pickerview
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.5
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
