//
//  ParticiparVC.swift
//  Comandato
//
//  Created by Leonel Sánchez on 22/02/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import PureJsonSerializer
import Kingfisher
import AVFoundation

class ParticiparVC: UIViewController, CDCircleDataSource, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var imgFondo: UIImageView!
    @IBOutlet weak var collectionMonedas: UICollectionView!
    @IBOutlet weak var centroRuleta: UIImageView!
    @IBOutlet weak var btnJugar: UIButton!
    @IBOutlet weak var gifMonedas: UIImageView!
    @IBOutlet weak var lblPremioGanado: UILabel!
    
    //constantes
    struct colores_ruleta {
        static let color_verde = UIColor(red: 193/255, green: 212/255, blue: 132/255, alpha: 1)
        static let color_naranja = UIColor(red: 230/255, green: 127/255, blue: 43/255, alpha: 1)
        static let color_morado = UIColor(red: 158/255, green: 65/255, blue: 144/255, alpha: 1)
        static let color_morado2 = UIColor(red: 118/255, green: 106/255, blue: 168/255, alpha: 1)
    }
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var laRuleta : CDCircle = CDCircle()
    var overlay : CDCircleOverlayView = CDCircleOverlayView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var arrColores = [UIColor]()
    var timer = NSTimer()
    var girandoRuleta = false
    var itemsRuleta = [itemRuleta]()
    var itemsMonedas = [Moneda]()
    var itemGanador = itemRuleta()
    var maxIndice = 0
    var lblGanadora = UILabel()
    let reuseIdentifier = "celdaMoneda"
    var espiga = UIImageView()
    var player: AVAudioPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.startActivityIndicator()
            
            //setear el logo en la navigation bar
            let bannerImg = UIImage(named: "logo_banner")
            let bannerImgView = UIImageView(image: bannerImg)
            bannerImgView.frame = CGRectMake(0, 0, 179, 13)
            bannerImgView.contentMode = UIViewContentMode.ScaleAspectFit
            self.navigationItem.titleView = bannerImgView
            self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
            
            //boton para cerrar sesion
            var imgCerrarSesion = UIImage(named: "cerrarsesion70")
            imgCerrarSesion = imgCerrarSesion?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgCerrarSesion, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.cerrarSession))
        })
        self.view.sendSubviewToBack(self.imgFondo)
        
        //Prod
        self.btnJugar.addTarget(self, action: #selector(self.giraRuleta), forControlEvents: .TouchUpInside)
        
        //pruebas
        //self.btnJugar.addTarget(self, action: #selector(self.pruebaBtnRuleta), forControlEvents: .TouchUpInside)
        
        self.collectionMonedas.dataSource = self
        self.collectionMonedas.delegate = self
        
        self.lblPremioGanado.alpha = 0
    }
    
    override func viewDidAppear(animated: Bool) {
        self.arrColores.append(colores_ruleta.color_verde)
        self.arrColores.append(colores_ruleta.color_naranja)
        self.arrColores.append(colores_ruleta.color_morado)
        self.arrColores.append(colores_ruleta.color_morado2)
        
        let mitadAnchoPantalla = self.view.frame.size.width / 2.5
        let mitadAltoPantalla = self.view.frame.size.height / 2.5
        let espacioCentro = (mitadAnchoPantalla - self.centroRuleta.frame.size.width) - 10
        
        laRuleta = CDCircle(frame: CGRectMake(50, 150, mitadAnchoPantalla, mitadAltoPantalla), numberOfSegments: 8, ringWidth: espacioCentro)
        
        laRuleta.dataSource = self
        
        self.overlay = CDCircleOverlayView(circle: laRuleta)
        //let colores_usados = [UIColor]()
        var indiceColor = Int(arc4random_uniform(UInt32(self.arrColores.count)))
        for indice in 0 ... (laRuleta.thumbs.count - 1){
            let thumb = laRuleta.thumbs[indice] as! CDCircleThumb
            if (indiceColor > (self.arrColores.count - 1)){
                indiceColor = 0
            }
            thumb.arcColor = self.arrColores[indiceColor]
            thumb.separatorColor = UIColor.whiteColor()
            thumb.separatorStyle = CDCircleThumbsSeparatorBasic
            thumb.gradientFill = false
            thumb.tag = indice
            laRuleta.thumbs[indice] = thumb
            indiceColor += 1
        }
        let thumb = laRuleta.thumbs[0] as! CDCircleThumb
        
        espiga = UIImageView(frame: CGRectMake(self.centroRuleta.frame.origin.x + (self.centroRuleta.frame.size.width / 2), self.centroRuleta.frame.origin.y, 20, 20))
        espiga.center = self.centroRuleta.center
        var espigaFrame = espiga.frame
        espigaFrame = CGRectMake(espigaFrame.origin.x, espigaFrame.origin.y - (thumb.frame.size.height), espigaFrame.size.width, espigaFrame.size.height)
        espiga.frame = espigaFrame
        espiga.image = UIImage(named: "flechaRuleta")
        
        self.cargaDatosruleta()
    }
    
    func pruebaBtnRuleta(){
        
        let animacion = CABasicAnimation(keyPath: "transform.rotation.z")
        animacion.fromValue = 0.0
        animacion.toValue = 2*M_PI
        animacion.duration = 1.0
        animacion.repeatDuration = 9999999
        self.laRuleta.layer.addAnimation(animacion, forKey: "SpinAnimation")
        self.playSoundRuleta("beat", extensionArchivo: "wav")
        
        
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(self.detenerRuleta), userInfo: nil, repeats: false)
        
        /*
        let elGifMonedas = UIImage.gifImageWithName("gifMonedas")
        self.gifMonedas.image = elGifMonedas
        self.gifMonedas.hidden = false
        
        
        var centrolbl = self.lblPremioGanado.center
        centrolbl.y -= 20
        
        UIView.animateWithDuration(2, animations: {
            self.lblPremioGanado.center = centrolbl
            self.lblPremioGanado.alpha = 1
        })
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(3.2, target: self, selector: #selector(self.quitarGif), userInfo: nil, repeats: false)
        
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(self.backToPrincipal), userInfo: nil, repeats: false)
 */
    }
    
    func playSoundRuleta(nombreArchivo: String, extensionArchivo: String) {
        let url = NSBundle.mainBundle().URLForResource(nombreArchivo, withExtension: extensionArchivo)!
        
        do {
            player = try AVAudioPlayer(contentsOfURL: url)
            guard let player = player else { return }
            
            player.prepareToPlay()
            player.play()
            player.numberOfLoops = 999
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    
    func giraRuleta (){
        if (!self.girandoRuleta) {
            let animacion = CABasicAnimation(keyPath: "transform.rotation.z")
            animacion.fromValue = 0.0
            animacion.toValue = 2*M_PI
            animacion.duration = 1.0
            animacion.repeatDuration = 9999999
            self.laRuleta.layer.addAnimation(animacion, forKey: "SpinAnimation")
            
            self.girandoRuleta = true
            self.playSoundRuleta("beat", extensionArchivo: "wav")
        } else {
            return
        }
        
        var dtosJson = "{\"metodo\":\"obtenerItemGanadorRuletaUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los premios disponibles", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    if (resultado == "error"){
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar obtener los premios disponibles"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.itemGanador.item_id = json["lista"]!["itemGanador"]!["item_id"]!.stringValue!
                        self.itemGanador.cantidad = json["lista"]!["itemGanador"]!["cantidad"]!.stringValue!
                        self.itemGanador.probabilidad = Double(json["lista"]!["itemGanador"]!["probabilidad"]!.stringValue!)!
                        self.itemGanador.puntos = json["lista"]!["itemGanador"]!["puntos"]!.stringValue!
                        self.itemGanador.punto_id = json["lista"]!["itemGanador"]!["punto_id"]!.stringValue!
                        self.itemGanador.url_imagen = json["lista"]!["itemGanador"]!["url_imagen"]!.stringValue!
                        
                        self.lblGanadora.text = "\(self.itemGanador.cantidad) \(self.itemGanador.puntos)"
                        
                        self.itemsMonedas.removeAll()
                        for item in (json["lista"]!["totalPuntosPosterior"]!.arrayValue)!{
                            let unaMoneda = Moneda()
                            unaMoneda.nombre = (item["nombre"]?.stringValue)!.uppercaseString
                            unaMoneda.punto_id = (item["punto_id"]?.stringValue)!
                            unaMoneda.usuario_id = (item["usuario_id"]?.stringValue)!
                            if (item["punto_padre_id"]?.stringValue) != nil {
                                unaMoneda.punto_padre_id = (item["punto_padre_id"]?.stringValue)!
                            } else {
                                unaMoneda.punto_padre_id = ""
                            }
                            
                            unaMoneda.url_imagen = (item["url_imagen"]?.stringValue)!
                            unaMoneda.total = (item["total"]?.stringValue)!
                            self.itemsMonedas.append(unaMoneda)
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(self.detenerRuleta), userInfo: nil, repeats: false)
                            self.collectionMonedas.reloadData()
                            self.collectionMonedas.setNeedsLayout()
                            self.collectionMonedas.layoutIfNeeded()
                            self.collectionMonedas.layoutSubviews()
                        })
                        
                    } else {
                        
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
        
        
    }
    
    func detenerRuleta(){
        self.laRuleta.layer.removeAllAnimations()
        self.player?.stop()
        self.playSoundRuleta("sonido_monedas", extensionArchivo: "mp3")
        var centrolbl = self.lblPremioGanado.center
        centrolbl.y -= 20
        
        UIView.animateWithDuration(2, animations: {
            self.lblPremioGanado.center = centrolbl
            self.lblPremioGanado.alpha = 1
        })
        
        let elGifMonedas = UIImage.gifImageWithName("gifMonedas")
        self.gifMonedas.image = elGifMonedas
        self.gifMonedas.hidden = false
        //self.view.bringSubviewToFront(self.gifMonedas)
        self.timer = NSTimer.scheduledTimerWithTimeInterval(3.2, target: self, selector: #selector(self.quitarGif), userInfo: nil, repeats: false)
        NSTimer.scheduledTimerWithTimeInterval(4.5, target: self, selector: #selector(self.backToPrincipal), userInfo: nil, repeats: false)
    }
    
    func backToPrincipal(){
        navigationController?.popViewControllerAnimated(true)
    }
    
    func quitarGif(){
        self.gifMonedas.hidden = true
        self.gifMonedas.image = nil
        self.player?.stop()
    }
    
    //MARK: Carga datos de la ruleta
    func cargaDatosruleta(){
        var dtosJson = "{\"metodo\":\"obtenerItemsRuletaUsuario\","
        dtosJson += "\"parametros\": [{"
        dtosJson += "\"nombre\": \"usuario_id\","
        dtosJson += "\"valor\": \"\(self.userDefaults.valueForKey(Constantes.JsonData.usuario.usuario_id) as! String)\""
        dtosJson += "}]"
        dtosJson += "}"
        
        
        //para el envio de datos
        let url:NSURL = NSURL(string: Constantes.WSData.urlRest)!
        let session = NSURLSession.sharedSession()
        
        let loginString = NSString(format: "%@:%@", Constantes.WSData.username, Constantes.WSData.password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = Constantes.WSData.metodoHTTP
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.HTTPBody = dtosJson.dataUsingEncoding(NSUTF8StringEncoding)
        
        //envio de datos
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            let httpResponse = response as? NSHTTPURLResponse
            
            if httpResponse?.statusCode != 200 {
                print(httpResponse?.statusCode)
            }
            
            if ( error != nil ) {
                print("Localized description error: \(error!.localizedDescription)")
                //quitar el loader
                dispatch_async(dispatch_get_main_queue()){
                    self.stopActivityIndicator()
                }
                
                let alertaError = UIAlertController(title: "", message: "Ocurrio un error al intentar obtener los premios disponibles", preferredStyle: .Alert)
                let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                    //No hacer nada por ahora
                }
                alertaError.addAction(okActionError)
                dispatch_async(dispatch_get_main_queue()){
                    self.presentViewController(alertaError, animated: true, completion: nil)
                }
                
                
            } else {
                do {
                    //deserializar los datos
                    let json = try Json.deserialize(data!)
                    let resultado = json["resultado"]?.stringValue ?? ""
                    if (resultado == "error"){
                        //quitar el loader
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                        }
                        
                        var texto = json["texto"]?.stringValue ?? "Ocurrio un error al intentar obtener los premios disponibles"
                        texto = String(UTF8String: texto.cStringUsingEncoding(NSUTF8StringEncoding)!)!
                        let alertaError = UIAlertController(title: "", message: texto, preferredStyle: .Alert)
                        let okActionError = UIAlertAction(title: "OK", style: .Cancel) { action in
                            //No hacer nada por ahora
                        }
                        alertaError.addAction(okActionError)
                        dispatch_async(dispatch_get_main_queue()){
                            self.presentViewController(alertaError, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if (resultado == "ok"){
                        
                        self.itemsRuleta.removeAll()
                        self.itemsMonedas.removeAll()
                        if (!(json["lista"]?.isNull)!){
                            if (!((json["lista"]!["itemsRuleta"]?.isNull)!) && !((json["lista"]!["totalPuntosAnterior"]?.isNull)!)) {
                                for item in (json["lista"]!["itemsRuleta"]!.arrayValue)! {
                                    let unItem = itemRuleta()
                                    unItem.item_id = (item["item_id"]?.stringValue)!
                                    unItem.cantidad = (item["cantidad"]?.stringValue)!
                                    unItem.probabilidad = Double((item["probabilidad"]?.stringValue)!)!
                                    unItem.puntos = (item["puntos"]?.stringValue)!
                                    unItem.punto_id = (item["punto_id"]?.stringValue)!
                                    unItem.url_imagen = (item["url_imagen"]?.stringValue)!
                                    
                                    self.itemsRuleta.append(unItem)
                                }
                                
                                
                                for item in (json["lista"]!["totalPuntosAnterior"]!.arrayValue)!{
                                    let unaMoneda = Moneda()
                                    unaMoneda.nombre = (item["nombre"]?.stringValue)!.uppercaseString
                                    unaMoneda.punto_id = (item["punto_id"]?.stringValue)!
                                    unaMoneda.usuario_id = (item["usuario_id"]?.stringValue)!
                                    if (item["punto_padre_id"]?.stringValue) != nil {
                                        unaMoneda.punto_padre_id = (item["punto_padre_id"]?.stringValue)!
                                    } else {
                                        unaMoneda.punto_padre_id = ""
                                    }
                                    
                                    unaMoneda.url_imagen = (item["url_imagen"]?.stringValue)!
                                    unaMoneda.total = (item["total"]?.stringValue)!
                                    self.itemsMonedas.append(unaMoneda)
                                }
                                
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.ponerEtiqueasRuleta()
                                    self.collectionMonedas.reloadData()
                                    self.collectionMonedas.setNeedsLayout()
                                    self.collectionMonedas.layoutIfNeeded()
                                    self.collectionMonedas.layoutSubviews()
                                })
                            } else {
                                NSLog("Se consulto correctamente los items de la ruleta, la lista no es null, pero itemsRuleta o puntosAnterior es null")
                            }
                        } else {
                            NSLog("Se consulto correctamente los items de la ruleta, pero la lista llego null")
                        }
                        
                    } else {
                        
                        //quitar el loader
                        dispatch_async(dispatch_get_main_queue()){
                            self.stopActivityIndicator()
                        }
                        //si el resultado no es ok o error ha ocurrido un problema en la respuesta del servidor
                        //enviar alerta de error
                        print("Ocurrio un error de otro tipo, este es el resultado: \(resultado)")
                    }
                    
                } catch {
                    print("Json serialization failed with error: \(error)")
                }
                
            }
        } //hasta aqui el data task request
        
        task.resume()
        
        //hasta aca envio de datos
    }

    //MARK poner etiquetas a ruleta
    func ponerEtiqueasRuleta(){
        self.maxIndice = self.getIndexMaxProb()
        
        var thumb = laRuleta.thumbs[0] as! CDCircleThumb
        self.lblGanadora = UILabel(frame: CGRectMake(-5, 0, 50, 10))
        self.lblGanadora.numberOfLines = 1
        self.lblGanadora.text = "\(self.itemsRuleta[maxIndice].cantidad) \(self.itemsRuleta[maxIndice].puntos)"
        self.lblGanadora.textColor = UIColor.whiteColor()
        //ajustar el tipo de fuente dependiendo del tamaño de pantalla
        self.lblGanadora.font = UIFont.boldSystemFontOfSize(10)
        self.lblGanadora.transform = CGAffineTransformMakeRotation(-CGFloat(M_PI_2))
        thumb.iconView.addSubview(self.lblGanadora)
        laRuleta.thumbs[0] = thumb
        
        for indice in 1 ... (laRuleta.thumbs.count - 1){
            thumb = laRuleta.thumbs[indice] as! CDCircleThumb
            let label = UILabel(frame: CGRectMake(-5, 0, 50, 10))
            label.numberOfLines = 1
            label.text = "\(self.itemsRuleta[indice].cantidad) \(self.itemsRuleta[indice].puntos)"
            label.textColor = UIColor.whiteColor()
            //ajustar el tipo de fuente dependiendo del tamaño de pantalla
            label.font = UIFont.boldSystemFontOfSize(10)
            label.transform = CGAffineTransformMakeRotation(-CGFloat(M_PI_2))
            thumb.iconView.addSubview(label)
            laRuleta.thumbs[indice] = thumb
        }
        
        dispatch_async(dispatch_get_main_queue()){
            self.stopActivityIndicator()
            self.view.addSubview(self.laRuleta)
            self.view.addSubview(self.overlay)
            self.laRuleta.center = self.view.center
            self.centroRuleta.center = self.laRuleta.center
            self.view.bringSubviewToFront(self.centroRuleta)
            self.centroRuleta.hidden = false
            self.view.addSubview(self.espiga)
            self.view.bringSubviewToFront(self.espiga)
        }

    }
    
    func getIndexMaxProb()->Int{
        var index = 0
        var max = 0.0
        for indice in 0 ... (self.itemsRuleta.count - 1) {
            let prob = Double(self.itemsRuleta[indice].probabilidad)
            if (  prob > max ){
                max = prob
                index = indice
            }
        }
        return index
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsMonedas.count
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.reuseIdentifier, forIndexPath: indexPath) as! CeldaMonedaCV
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        let imgURL = NSURL(string: self.itemsMonedas[indexPath.item].url_imagen)
        cell.imgMoneda.kf_setImageWithURL(imgURL)
        cell.lblCantidadMonedas.text = self.itemsMonedas[indexPath.item].total
        
        cell.needsUpdateConstraints()
        cell.layoutIfNeeded()
        cell.layoutSubviews()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let ancho = self.collectionMonedas.frame.width / 3.5
        return CGSizeMake(ancho, self.collectionMonedas.frame.height + 20)
    }

    func circle(circle: CDCircle!, iconForThumbAtRow row: Int) -> UIImage! {
        return UIImage()
    }
    
    //MARK: Metodos activity indicator
    func startActivityIndicator() {
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
        activityIndicator.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        // Change background color and alpha channel here
        activityIndicator.backgroundColor = UIColor.blackColor()
        activityIndicator.clipsToBounds = true
        activityIndicator.alpha = 0.8
        
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    //MARK: Cerrar sesion
    ///alerta antes de cerrar sesion
    func cerrarSession(sender: UIBarButtonItem){
        let refreshAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "SI", style: .Default, handler: { (action: UIAlertAction!) in
            
            let xfunciones = funciones()
            xfunciones.BorraDatosUsuario()
            self.navigationController?.presentViewController((self.storyboard?.instantiateViewControllerWithIdentifier(Constantes.IDgotoLogin))!, animated: true, completion: nil)
            
        }))
        
        // Tecnicamente no hace nada si se cancela la accion de cerrar sesion
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: { (action: UIAlertAction!) in
            //
            
        }))
        
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }

} // fin de clase


class itemRuleta {
    var item_id = ""
    var cantidad = ""
    var probabilidad = 0.0
    var puntos = ""
    var punto_id = ""
    var url_imagen = ""
}
