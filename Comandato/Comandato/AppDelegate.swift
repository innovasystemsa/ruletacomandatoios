//
//  AppDelegate.swift
//  Comandato
//
//  Created by Leonel Sánchez on 10/01/17.
//  Copyright © 2017 Firewall Soluciones. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    //datos de usuario
    let userDefaults = NSUserDefaults.standardUserDefaults()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch
        
        //############## FIREBASE ##################
        
        registerForPushNotifications(application)
        
        FIRApp.configure()
        
        NSNotificationCenter
            .defaultCenter()
            .addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification),
                         name: kFIRInstanceIDTokenRefreshNotification, object: nil)
        
        //############ FIREBASE END ################
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        let navBackgroundImage:UIImage! = UIImage(named: "toolbarback74")
        UINavigationBar.appearance().setBackgroundImage(navBackgroundImage, forBarMetrics: .Default)
        
        Fabric.with([Crashlytics.self])
        if ((self.userDefaults.valueForKey(Constantes.JsonData.base.usrLogueado)) != nil){
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initialVC : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("NavToPrincipal") as UIViewController
            self.window?.rootViewController = initialVC
            self.window?.makeKeyAndVisible()
        }
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    //############## FIREBASE ##################
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.currentNotificationCenter().delegate = self
            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions([.Badge, .Sound, .Alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.sharedApplication().registerForRemoteNotifications()
                }
                else{
                    print("no se dieron permisos a la app")
                }
            })
        }
            
        else{
            let notificationSettings = UIUserNotificationSettings(
                forTypes: [.Badge, .Sound, .Alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
        }
        
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
            FIRMessaging.messaging().subscribeToTopic("/topics/notificacion_general_ios")
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for i in 0..<deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        //Pruebas
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Sandbox)
        
        //prod
        
    }
    
    // [START receive_message]
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        
        print("willPresentNotification: \(notification)")
        let userInfo = notification.request.content.userInfo as NSDictionary
        print("\(userInfo)")
        
        let json = JSON(userInfo)
        let body = json["aps"]["alert"]["body"].string!
        let title = json["aps"]["alert"]["title"].string!
        
        let alerta = UIAlertController(title: title, message: body, preferredStyle: .Alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .Default,handler: nil)
        alerta.addAction(alertActionOK)
        self.window?.rootViewController?.presentViewController(alerta, animated: true, completion: nil)
        
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
        
        print("didReceiveNotificationResponse \(response)")
        
        let userInfo = response.notification.request.content
        let alerta = UIAlertController(title: userInfo.title, message: userInfo.body, preferredStyle: .Alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .Default,handler: nil)
        alerta.addAction(alertActionOK)
        self.window?.rootViewController?.presentViewController(alerta, animated: true, completion: nil)
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                     fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        
        print("notification:\(userInfo)")
        var title:String = ""
        var body:String = ""
        
        
        
        if #available(iOS 10.0, *){
            
            let json = JSON(userInfo)
            body = json["aps"]["alert"]["body"].string!
            title = json["aps"]["alert"]["title"].string!
            print (json)
        } else {
                let json = JSON(userInfo)
                body = json["aps"]["alert"]["body"].string!
                title = json["aps"]["alert"]["title"].string!
                print (json)
        }
        
        let alerta = UIAlertController(title: title, message: body, preferredStyle: .Alert)
        let alertActionOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),style: .Default,handler: nil)
        alerta.addAction(alertActionOK)
        self.window?.rootViewController?.presentViewController(alerta, animated: true, completion: nil)
        
    }
    
    
    // [END receive_message]
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM 
        connectToFcm()
    }
    // [END refresh_token]
    
    
    // [START connect_to_fcm]
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    // [END connect_to_fcm]
    
    // [START connect_on_active]
    func applicationDidBecomeActive(application: UIApplication) {
        connectToFcm()
    }
    // [END connect_on_active]
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    // [END disconnect_from_fcm]
    
    //############## FIREBASE ##################
    
    //Facebook
    func application(application: UIApplication,
                     openURL url: NSURL,
                             sourceApplication: String?,
                             annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            openURL: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    


}

// [START ios_10_data_message_handling]
extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(remoteMessage: FIRMessagingRemoteMessage) {
        
    }
}
// [END ios_10_data_message_handling]
